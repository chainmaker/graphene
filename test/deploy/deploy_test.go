/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package deploy

import (
	"context"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"chainmaker.org/chainmaker/chainmaker-graphene/common/tee"
	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/asym/rsa"
	"chainmaker.org/chainmaker/common/v2/crypto/sym/aes"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/common/v2/random/uuid"
	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/tee"

	"encoding/pem"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"

	//"io/ioutil"
	"testing"
)

var (
	configName = "config"
	configType = "json"
	configPath = "../../configs"
	orgId      = "wx-org1.chainmaker.org"
)

type factParams struct {
	FileName string `json:"file_name"`
	FileHash string `json:"file_hash"`
	Time     string `json:"time"`
}

func TestDeployContract(t *testing.T) {

	// init viper
	//testViperInitialization(t, configName, configType, configPath)

	// read contract code and cert file
	// contractCode := testViperReadFile(t, "contract.path")
	// certFile := testViperReadFile(t, "cert.path")
	filePath := "../../internal/vm-wasm/test-wasm/rust-func-verify-2.0.0.wasm"
	contractCode, err := ioutil.ReadFile(filePath)
	require.Nil(t, err)
	filePath = "../../configs/in_teecert.pem"
	certFile, err := ioutil.ReadFile(filePath)
	require.Nil(t, err)

	// get contract code hash
	contractHash := sha256.Sum256(contractCode)

	// random aes key generation
	aesKey := testAesKeyGeneration(t)

	// retrieve enc pubkey
	pubKeyPath := "../../configs/publickeyExt.pem"
	pubkeyExt, err := info.GetPubKey(pubKeyPath)
	require.Nil(t, err)

	//encPubKeyPathBytes := testViperReadFile(t, "crypto.encryptPubKey")
	//pubKey := &rsa.PublicKey{}
	//err := json.Unmarshal(encPubKeyPathBytes, &pubKey)

	// encrypt aes key
	require.Nil(t, err)
	symKeyCipher, err := pubkeyExt.(crypto.EncryptKey).Encrypt(aesKey.Key)
	require.Nil(t, err)

	//symKeyCipher := testPubkeyEncrypt(t, pubKey, aesBytes)

	// encrypt params
	params := &factParams{
		FileName: "name007",
		FileHash: "ab3456df5799b87c77e7f88",
		Time:     "6543234",
	}
	paramsBytes, err := json.Marshal(params)
	require.Nil(t, err)
	paramsCipher := testAESEncrypt(t, &aesKey, paramsBytes)

	// start enclave client
	connEnclave := testNewConn(t, "50053")
	defer connEnclave.Close()
	enclaveClient := tee.NewEnclaveServerClient(connEnclave)

	// init contract
	testInitContract(t, symKeyCipher, paramsCipher, contractHash[:], contractCode, certFile, enclaveClient)

	// invoke contract - save file
	testInvokeContract(t, symKeyCipher, paramsCipher, contractHash[:], contractCode, certFile, "save", enclaveClient)

	// invoke contract - get file
	//testInvokeContract(t, symKeyCipher, paramsCipher, contractHash[:], contractCode, certFile, "find_by_file_hash", enclaveClient)
	fmt.Println("start remote")
	testGetRemote(t, symKeyCipher, paramsCipher, contractHash[:], contractCode, certFile, enclaveClient)
}

// test random aes key generation
func testAesKeyGeneration(t *testing.T) aes.AESKey {
	key := make([]byte, 32)
	_, err := rand.Read(key)
	require.Nil(t, err)
	return aes.AESKey{Key: key}
}

// test random aes key generation
func testViperInitialization(t *testing.T, configName string, configType string, configPath string) {
	viper.SetConfigName(configName)
	viper.SetConfigType(configType)
	viper.AddConfigPath(configPath)
	err := viper.ReadInConfig()
	require.Nil(t, err)
}

// test file reading by viper
func testViperReadFile(t *testing.T, filename string) []byte {
	filepath := viper.GetString(filename)
	filebytes, err := ioutil.ReadFile(filepath)
	require.Nil(t, err)
	return filebytes
}

// test pubkey encrypt
func testPubkeyEncrypt(t *testing.T, pubkey *rsa.PublicKey, plainBytes []byte) []byte {
	cipher, err := pubkey.Encrypt(plainBytes)
	require.Nil(t, err)
	return cipher
}

// test ase encrypt
func testAESEncrypt(t *testing.T, aeskey *aes.AESKey, plainBytes []byte) []byte {
	cipher, err := aeskey.Encrypt(plainBytes)
	require.Nil(t, err)
	return cipher
}

// test new grpc connection
func testNewConn(t *testing.T, port string) *grpc.ClientConn {
	conn, err := grpc.Dial("localhost:"+port, grpc.WithInsecure())
	require.Nil(t, err)
	return conn
}

// test init_contract
func testInitContract(t *testing.T, symKeyCipher []byte, paramsCipher []byte, contractHash []byte, contractCode []byte, certFile []byte, enclaveClient tee.EnclaveServerClient) {
	txRequest := getTxRequest(symKeyCipher, paramsCipher, contractHash, contractCode, certFile, "init_contract")
	deployResponse, err := enclaveClient.DeployContract(context.Background(), txRequest)
	if err != nil {
		panic(err)
	}
	require.Nil(t, err)
	require.Equal(t, uint32(0), deployResponse.EnclaveResponsePayload.ContractResult.Code)
	testVerifySignature(t, deployResponse.EnclaveResponsePayload, deployResponse.Signature)
}

// test invoke_contract
func testInvokeContract(t *testing.T, symKeyCipher []byte, paramsCipher []byte, contractHash []byte, contractCode []byte, certFile []byte, method string, enclaveClient tee.EnclaveServerClient) {
	txRequest := getTxRequest(symKeyCipher, paramsCipher, contractHash, contractCode, certFile, method)
	invokeResponse, err := enclaveClient.InvokeContract(context.Background(), txRequest)
	require.Nil(t, err)
	require.Equal(t, uint32(0), invokeResponse.EnclaveResponsePayload.ContractResult.Code)
	testVerifySignature(t, invokeResponse.EnclaveResponsePayload, invokeResponse.Signature)

	// connect to blockchain
	connOcall := testNewConn(t, "50052")
	defer connOcall.Close()
	clientOcall := tee.NewEnclaveOutCallServerClient(connOcall)
	for _, v := range invokeResponse.EnclaveResponsePayload.TxRwset.TxWrites {
		_, err := clientOcall.OutCallPut(context.Background(),
			&tee.OutCallPutRequest{Key: string(v.Key), Value: v.Value, ContractName: v.ContractName})
		if err != nil {
			panic(err)
		}

		require.Nil(t, err)
	}
}

// test signature verification
func testVerifySignature(t *testing.T, payload *tee.EnclaveResponsePayload, signature []byte) {
	pubkey, err := info.GetPubKey(viper.GetString("crypto.signPubKey"))
	//signPubKeyPathBytes := testViperReadFile(t, "crypto.signPubKey")
	//verifyKey, err := asym.PublicKeyFromPEM(signPubKeyPathBytes)
	require.Nil(t, err)

	enclaveResponseBytes, err := payload.Marshal()
	require.Nil(t, err)

	certBytes, err := ioutil.ReadFile(viper.GetString("cert.path"))
	require.Nil(t, err)

	certBlock, _ := pem.Decode(certBytes)
	cert, err := bcx509.ParseCertificate(certBlock.Bytes)
	require.Nil(t, err)

	hashAlgo, err := bcx509.GetHashFromSignatureAlgorithm(cert.SignatureAlgorithm)
	require.Nil(t, err)

	opt := &crypto.SignOpts{
		Hash: hashAlgo,
		UID:  crypto.CRYPTO_DEFAULT_UID,
	}
	switch cert.SignatureAlgorithm {
	case bcx509.SHA256WithRSAPSS, bcx509.SHA384WithRSAPSS, bcx509.SHA512WithRSAPSS:
		opt.EncodingType = rsa.RSA_PSS
	}
	_, err = pubkey.VerifyWithOpts(enclaveResponseBytes, signature, opt)
	//_, err = verifyKey.VerifyWithOpts(enclaveResponseBytes, signature, optSM3)
	require.Nil(t, err)
}

func getTxRequest(symKeyCipher []byte, paramsCipher []byte, contractHash []byte, contractCode []byte, certFile []byte, method string) *common.TxRequest {
	txRequest := &common.TxRequest{
		Payload: &common.Payload{
			Parameters: []*common.KeyValuePair{
				{
					Key:   "SymKeyCipher",
					Value: symKeyCipher,
				},
				{
					Key:   "ParamsCipher",
					Value: paramsCipher,
				},
				{
					Key:   "ContractHash",
					Value: contractHash,
				},
				{
					Key:   "ContractCode",
					Value: contractCode,
				},
				{
					Key:   "ContractVersion",
					Value: []byte("test_version_0.0.0"),
				},
			},
			ChainId:      "chain01",
			TxType:       common.TxType_INVOKE_CONTRACT,
			TxId:         uuid.GetUUID() + uuid.GetUUID(),
			ContractName: "fact",
			Method:       method,
		},
		Sender: &common.EndorsementEntry{
			Signer: &accesscontrol.Member{
				OrgId:      orgId,
				MemberType: accesscontrol.MemberType_CERT,
				MemberInfo: certFile,
			},
			Signature: nil,
		},
	}
	return txRequest
}

func testGetRemote(t *testing.T, symKeyCipher []byte, paramsCipher []byte, contractHash []byte, contractCode []byte, certFile []byte, enclaveClient tee.EnclaveServerClient) {
	chall := &tee.RemoteAttestationRequest{"1123123"}
	k, err := enclaveClient.RemoteAttestationProve(context.Background(), chall)
	if err != nil {
		panic(err)
	}
	fmt.Println(k)
}
