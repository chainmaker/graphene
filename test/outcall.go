/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/tee"
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

type GatewayServer struct {
	simDB map[string][]byte
}

func (gs *GatewayServer) OutCallGet(ctx context.Context, request *tee.OutCallGetRequest) (*common.ContractResult, error) {
	contractRes := &common.ContractResult{}
	contractRes.Result = gs.simDB[request.Key]
	return contractRes, nil
}

func (gs *GatewayServer) OutCallPut(ctx context.Context, request *tee.OutCallPutRequest) (*common.ContractResult, error) {
	gs.simDB[request.Key] = request.Value
	return &common.ContractResult{}, nil
}

func main() {
	// start as a enclave server
	lis, err := net.Listen("tcp", "localhost:50052")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	// create grpc server
	grpcServer := grpc.NewServer()
	gs := &GatewayServer{}
	gs.simDB = make(map[string][]byte)
	tee.RegisterEnclaveOutCallServerServer(grpcServer, gs)
	reflection.Register(grpcServer)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
