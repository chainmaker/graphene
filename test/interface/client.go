/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"context"
	"log"
	"time"

	"google.golang.org/grpc"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/tee"
)

const (
	address = "localhost:50051"
)

func main() {
	//建立链接
	conn, err := grpc.Dial(address, grpc.WithInsecure())

	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	defer conn.Close()

	enclaveClient := tee.NewEnclaveServerClient(conn)

	// expiration time 3s
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	// Init enclave request
	initEnclaveResponse, err := enclaveClient.InitEnclave(ctx, &tee.InitEnclaveRequest{TeeCertSignAlg: "RSA"})
	if err != nil {
		log.Printf("init enclave failed: %v", err)
	}

	log.Printf("init enclave success: %s", initEnclaveResponse)

	// Deploy contract request
	deployResponse, err := enclaveClient.DeployContract(ctx, &common.TxRequest{})
	if err != nil {
		log.Printf("deploy contract failed: %v", err)
	}

	log.Printf("deploy contract success: %s", deployResponse)

	// Invoke contract request
	invokeResponse, err := enclaveClient.InvokeContract(ctx, &common.TxRequest{})
	if err != nil {
		log.Printf("invoke contract failed: %v", err)
	}

	log.Printf("invoke contract success: %s", invokeResponse)

	// Remote attestation prove request
	remoteAttestationResponse, err := enclaveClient.RemoteAttestationProve(ctx, &tee.RemoteAttestationRequest{Challenge: "random challenge"})
	if err != nil {
		log.Printf("remote attestation failed: %v", err)
	}

	log.Printf("remote attestation success: %s", remoteAttestationResponse.RemoteAttestationPayload.Challenge)
}
