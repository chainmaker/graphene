/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package crypto

import (
	"bytes"
	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/asym"
	"chainmaker.org/chainmaker/common/v2/crypto/asym/rsa"
	"chainmaker.org/chainmaker/common/v2/crypto/sym/aes"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"testing"
)

func TestSign(t *testing.T) {
	// crypto options
	opt := crypto.SM2
	digest := sha256.Sum256([]byte("js"))
	optSM3 := &crypto.SignOpts{
		Hash: crypto.HASH_TYPE_SM3,
		UID:  crypto.CRYPTO_DEFAULT_UID,
	}

	// generate sign key pair and simple store
	skPEM, pkPEM, err := asym.GenerateKeyPairPEM(opt)
	require.Nil(t, err)
	err = ioutil.WriteFile("../../configs/cert/sign_priv.key", []byte(skPEM), 0644)
	require.Nil(t, err)
	err = ioutil.WriteFile("../../configs/cert/sign_pub.key", []byte(pkPEM), 0644)
	require.Nil(t, err)

	// retrieve keys
	skStr, err := ioutil.ReadFile("../../configs/cert/sign_priv.key")
	require.Nil(t, err)
	pkStr, err := ioutil.ReadFile("../../configs/cert/sign_pub.key")
	require.Nil(t, err)

	// get sk/pk
	sk, err := asym.PrivateKeyFromPEM(skStr, nil)
	require.Nil(t, err)
	pk, err := asym.PublicKeyFromPEM(pkStr)
	require.Nil(t, err)

	// sign and verify
	sig, err := sk.SignWithOpts(digest[:], optSM3)
	require.Nil(t, err)
	ok, err := pk.VerifyWithOpts(digest[:], sig, optSM3)
	require.Nil(t, err)
	require.Equal(t, true, ok)
}

func TestEncrypt(t *testing.T) {

	// generate crypt key pair and simple store
	msg := "js"
	decKey, err := rsa.NewDecryptionKey(crypto.RSA3072)
	require.Nil(t, err)
	decKeyBytes, err := json.Marshal(decKey)
	require.Nil(t, err)
	err = ioutil.WriteFile("../../configs/cert/enc_priv.key", decKeyBytes, 0644)
	require.Nil(t, err)

	encKey := decKey.EncryptKey()
	encKeyBytes, err := json.Marshal(encKey)
	require.Nil(t, err)
	err = ioutil.WriteFile("../../configs/cert/enc_pub.key", encKeyBytes, 0644)
	require.Nil(t, err)

	// retrieve keys
	decKeyBytes, err = ioutil.ReadFile("../../configs/cert/enc_priv.key")
	require.Nil(t, err)
	err = json.Unmarshal(decKeyBytes, &decKey)
	require.Nil(t, err)

	encKeyBytes, err = ioutil.ReadFile("../../configs/cert/enc_pub.key")
	require.Nil(t, err)
	err = json.Unmarshal(encKeyBytes, &encKey)
	require.Nil(t, err)

	// encrypt and decrypt
	cipher, err := encKey.Encrypt([]byte(msg))
	require.Nil(t, err)
	require.NotNil(t, cipher)

	plain, err := decKey.Decrypt(cipher)
	require.Nil(t, err)
	require.NotNil(t, plain)
	require.True(t, bytes.Equal(plain, []byte(msg)))
}

func TestMixedEncrypt(t *testing.T) {

	// generate crypt key pair and simple store
	msg := "js"
	decKey, err := rsa.NewDecryptionKey(crypto.RSA3072)
	require.Nil(t, err)
	decKeyBytes, err := json.Marshal(decKey)
	require.Nil(t, err)
	err = ioutil.WriteFile("../../configs/cert/enc_priv.key", decKeyBytes, 0644)
	require.Nil(t, err)

	encKey := decKey.EncryptKey()
	encKeyBytes, err := json.Marshal(encKey)
	require.Nil(t, err)
	err = ioutil.WriteFile("../../configs/cert/enc_pub.key", encKeyBytes, 0644)
	require.Nil(t, err)

	// retrieve keys
	decKeyBytes, err = ioutil.ReadFile("../../configs/cert/enc_priv.key")
	require.Nil(t, err)
	err = json.Unmarshal(decKeyBytes, &decKey)
	require.Nil(t, err)

	encKeyBytes, err = ioutil.ReadFile("../../configs/cert/enc_pub.key")
	require.Nil(t, err)
	err = json.Unmarshal(encKeyBytes, &encKey)
	require.Nil(t, err)

	// encrypt and decrypt
	key := make([]byte, 32)
	_, err = rand.Read(key)
	require.Nil(t, err)
	aesKey := aes.AESKey{Key: key}
	aesBytes, err := json.Marshal(aesKey)
	require.Nil(t, err)

	keyCipher, err := encKey.Encrypt(aesBytes)
	require.Nil(t, err)
	require.NotNil(t, keyCipher)

	cipher, err := aesKey.Encrypt([]byte(msg))
	require.Nil(t, err)
	require.NotNil(t, keyCipher)

	keyPlain, err := decKey.Decrypt(keyCipher)
	require.Nil(t, err)
	aesPlainKey := &aes.AESKey{}
	err = json.Unmarshal(keyPlain, aesPlainKey)
	require.Nil(t, err)

	plain, err := aesPlainKey.Decrypt(cipher)
	require.NotNil(t, plain)
	require.True(t, bytes.Equal(plain, []byte(msg)))
}
