/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"os/signal"

	//"runtime"

	"time"

	"net"

	"chainmaker.org/chainmaker/chainmaker-graphene/internal/vm-wasm/logger"
	info "chainmaker.org/chainmaker/chainmaker-graphene/common/tee"
	"chainmaker.org/chainmaker/chainmaker-graphene/configs"
	"chainmaker.org/chainmaker/chainmaker-graphene/internal/service"
	"chainmaker.org/chainmaker/chainmaker-graphene/loggers"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/tee"
	"github.com/flyaways/pool"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	log   *logger.CMLogger
	isSim bool
)

func init() {
	flag.BoolVar(&isSim, "module", false, "is sim module?")
	// init viper for configs
	configs.Init("../../configs")
	loggers.Init()
	info.Init()
}

type EnclaveServer struct {
	// EnclaveServer
	ocallPool *pool.GRPCPool
}

// InitEnclave
func (es *EnclaveServer) InitEnclave(ctx context.Context, in *tee.InitEnclaveRequest) (*tee.InitEnclaveResponse, error) {

	log.Infof("Received enclave initialization request: %+v", in)

	report, csr, pub, err := service.Init(in.TeeCertSignAlg, in.TeeEncryptAlg, isSim)
	if err != nil {
		return nil, err
	}

	return &tee.InitEnclaveResponse{
		TeeReport: report,
		TeePubkey: pub,
		TeeCsr:    csr,
	}, nil
}

func (es *EnclaveServer) DeployContract(ctx context.Context, in *common.TxRequest) (*tee.EnclaveResponse, error) {

	log.Infof("Received deploy contract request, contract name is %s", in.Payload.ContractName)

	conn, err := es.ocallPool.Get()
	if err != nil {
		return nil, errors.New("Get connect from ocall pool err :" + err.Error())
	}

	if ctx.Err() == context.Canceled {
		return nil, errors.New("remote attestation prove canceled")
	}

	enclaveResponse, err := service.Deploy(ctx, in, conn)
	if err != nil {
		return nil, errors.New("Deploy contract err :" + err.Error())
	}

	if es.ocallPool.Put(conn) != nil {
		log.Infof("invoke contract ocall poll put conn fial:%#v\n", err)
		return nil, errors.New("invoke contract ocall poll put conn fial:%#v\n" + err.Error())
	}
	log.Infof("success deploy contract")

	return enclaveResponse, nil
}

func (es *EnclaveServer) InvokeContract(ctx context.Context, in *common.TxRequest) (*tee.EnclaveResponse, error) {
	log.Infof("Received invoke contract request, contract name is %s, method is %s", in.Payload.ContractName, in.Payload.Method)
	conn, err := es.ocallPool.Get()
	if err != nil {
		return nil, errors.New("Get connect from ocall pool err :" + err.Error())
	}

	if ctx.Err() == context.Canceled {
		return nil, errors.New("remote attestation prove canceled")
	}

	enclaveResponse, err := service.Invoke(ctx, in, conn)

	if err != nil {
		return nil, errors.New("Invoke contract err :" + err.Error())
	}

	if es.ocallPool.Put(conn) != nil {
		log.Infof("invoke contract ocall poll put conn fial:%#v\n", err)
		return nil, errors.New("invoke contract ocall poll put conn fial:%#v\n" + err.Error())
	}

	return enclaveResponse, nil
}

func (es *EnclaveServer) RemoteAttestationProve(ctx context.Context, in *tee.RemoteAttestationRequest) (*tee.RemoteAttestationResponse, error) {

	log.Infof("Received remote attestation prove request, challenge string is %s", in.Challenge)

	defer ctx.Done()
	if ctx.Err() == context.Canceled {
		return nil, errors.New("remote attestation prove canceled")
	}

	cert, report, sign, err := service.TeeRemoteAttestationProve(in.Challenge,isSim)
	if err != nil {
		return nil, err
	}

	return &tee.RemoteAttestationResponse{
		Signature: sign,
		RemoteAttestationPayload: &tee.RemoteAttestationPayload{
			Challenge: in.Challenge,
			Report:    report,
			TeeCert:   cert,
		},
	}, nil
}

func main() {
	//debug.SetGCPercent(-1)
	fmt.Println("启动")
	flag.Parse()
	log = loggers.GetLogger()
	viper := configs.GetConfig()
	service.InitVmManager(viper.GetString("chain.ID"))
	signAlgo := viper.GetString(`algo.signAlgo`)
	encAlgo := viper.GetString(`algo.encAlgo`)

	_, _, _, err := service.Init(signAlgo, encAlgo, isSim)
	if err != nil {
		log.Debugf("enclave init error,err = %s", err)
		panic(err)
	}
	log.Debug("enclave init success")
	// create an ocall client pool
	ocallPoolOptions := &pool.Options{
		InitTargets:  []string{viper.GetString("client.targetUrl")},
		InitCap:      viper.GetInt("client.initCapacity"),
		MaxCap:       viper.GetInt("client.maxCapacity"),
		DialTimeout:  time.Second * time.Duration(viper.GetInt("client.dialTimeout")),
		IdleTimeout:  time.Second * time.Duration(viper.GetInt("client.idleTimeout")),
		ReadTimeout:  time.Second * time.Duration(viper.GetInt("client.readTimeout")),
		WriteTimeout: time.Second * time.Duration(viper.GetInt("client.writeTimeout")),
	}
	ocallPool, err := pool.NewGRPCPool(ocallPoolOptions, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("failed to create rpc client pool: %v", err)
		return
	}
	if ocallPool == nil {
		log.Fatalf("ocall pool = %v", ocallPool)
		return
	}
	defer ocallPool.Close()
	// start as an enclave server
	lis, err := net.Listen("tcp", viper.GetString("server.port"))
	if err != nil {
		log.Fatal("failed to listen: %v", err)
	}
	// gc := GC{mutex: &mutex}

	// gc.selfGC()
	// gc.freeOSMemory()
	maxSize := 20 * 1024 * 1024
	// grpcServer := grpc.NewServer(grpc.MaxRecvMsgSize(maxSize), grpc.MaxSendMsgSize(maxSize))
	grpcServer := grpc.NewServer(grpc.MaxRecvMsgSize(maxSize), grpc.MaxSendMsgSize(maxSize))
	tee.RegisterEnclaveServerServer(grpcServer, &EnclaveServer{ocallPool: ocallPool})
	// Register reflection service on gRPC server.
	reflection.Register(grpcServer)

	fmt.Println("grpc server register success")
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
}
