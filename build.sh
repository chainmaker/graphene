#!/bin/bash
#
#
#Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
# # SPDX-License-Identifier: Apache-2.0
#
module=$1
debug=$2
name="enclave-server"
echo $name
source /etc/profile

# clear some files

if [ "$module" == "SGX=1" ]; then
    rm -rf ./configs/*.pem ./configs/*.dat ./logs/*.log.*
    echo "sgx module delete file sucess"
elif [ "$module" == "SIM" ]; then
    module=$""
    #sim module need save report file and key_request file
    rm -rf ./configs/*.pem ./logs/*.log.*
    echo "SIM module delete file sucess"
    echo $module
else
    echo "moudle must be value of SGX=1 or SIM"
    exit
fi
make clean
# touch a new empty file
#touch ./configs/in_teecert.pem && chmod 666 ./configs/in_teecert.pem

goBuild() {
    go build -o "../$name"
    if [ $? -eq 0 ]; then
        echo "$name build success"
    else
        echo "$name build faild"
        exit 1;
    fi
}

cd ./cmd && goBuild && cd ../

make $module $debug
if [ $? -eq 0 ]; then
    echo "sgx build success"
else
    echo "sgx build faild"
    exit 1
fi

echo " build success"

# # new enclave cert
# cd ./common/info/ && go test -v -run TestGenEnclaveCert init_test.go

# make SGX=1 $debug
