/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package configs

import (
	"github.com/spf13/viper"
)

// viperConfig 全局配置变量
var viperConfig *viper.Viper

func Init(path string) {
	// read configs
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	viper.AddConfigPath(path)
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			panic(err)
		} else {
			panic(err)
		}
	}
	viperConfig = viper.GetViper()
}

// GetConfig 获取全局配置
func GetConfig() *viper.Viper {
	return viperConfig
}
