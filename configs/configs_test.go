/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/
package configs

import (
	"testing"

	"github.com/go-playground/assert/v2"
)

func TestGetConfig(t *testing.T) {
	Init("./")
	vi := GetConfig()
	err := vi.ReadInConfig()
	if err != nil {
		panic(err)
	}
	k := vi.GetString("report.path")
	assert.NotEqual(t, k, "")
}
