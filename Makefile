ARCH_LIBDIR ?= /lib/$(shell $(CC) -dumpmachine)
ENTRY_POINT ?= ./enclave-server

SGX_SIGNER_KEY ?= ./enclave-key.pem

ifeq ($(DEBUG),1)
GRAMINE_LOG_LEVEL = debug
else
GRAMINE_LOG_LEVEL = error
endif

.PHONY: all
all: enclave-server.manifest
ifeq ($(SGX),1)
all: enclave-server.manifest.sgx enclave-server.sig enclave-server.token
endif

enclave-server.manifest: enclave-server.manifest.template
	gramine-manifest \
		-Dlog_level=$(GRAMINE_LOG_LEVEL) \
		-Darch_libdir=$(ARCH_LIBDIR) \
		-Dentrypoint=$(ENTRY_POINT) \
		$< >$@

enclave-server.manifest.sgx: enclave-server.manifest
	@test -s $(SGX_SIGNER_KEY) || \
	    { echo "SGX signer private key was not found, please specify SGX_SIGNER_KEY!"; exit 1; }
	gramine-sgx-sign \
		--key $(SGX_SIGNER_KEY) \
		--manifest $< \
		--output $@

enclave-server.sig: enclave-server.manifest.sgx

enclave-server.token: enclave-server.sig
	gramine-sgx-get-token --output $@ --sig $<

.PHONY: check
check: all
	./run-tests.sh > TEST_STDOUT 2> TEST_STDERR
	@grep -q "Success 1/4" TEST_STDOUT
	@grep -q "Success 2/4" TEST_STDOUT
	@grep -q "Success 3/4" TEST_STDOUT
	@grep -q "Success 4/4" TEST_STDOUT

.PHONY: clean
clean:
	$(RM) *.manifest *.manifest.sgx *.token *.sig OUTPUT* *.PID TEST_STDOUT TEST_STDERR
	$(RM) -r scripts/__pycache__

.PHONY: distclean
distclean: clean
