/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/
package loggers

import (

	"chainmaker.org/chainmaker/chainmaker-graphene/internal/vm-wasm/logger"
	"chainmaker.org/chainmaker/chainmaker-graphene/configs"
)

var log *logger.CMLogger

func Init() {
	SetLogger()
}

func SetLogger() {
	viper := configs.GetConfig()

	defaultLogNode := logger.LogNodeConfig{
		LogLevelDefault: viper.GetString(`log.logLevels`),
		FilePath:        viper.GetString(`log.filePath`),
		MaxAge:          viper.GetInt(`log.maxAge`),
		RotationTime:    viper.GetInt(`log.rotationTime`),
		RotationSize:    viper.GetInt64(`log.rotationSize`),
		LogInConsole:    viper.GetBool(`log.logInConsole`),
		ShowColor:       viper.GetBool(`log.showColor`),
	}

	config := &logger.LogConfig{
		SystemLog: logger.LogNodeConfig{
			LogLevelDefault: defaultLogNode.LogLevelDefault,
			FilePath:        defaultLogNode.FilePath,
			MaxAge:          defaultLogNode.MaxAge,
			RotationTime:    defaultLogNode.RotationTime,
			RotationSize:    defaultLogNode.RotationSize,
			LogInConsole:    defaultLogNode.LogInConsole,
		},
		BriefLog: logger.LogNodeConfig{
			LogLevelDefault: defaultLogNode.LogLevelDefault,
			FilePath:        defaultLogNode.FilePath,
			MaxAge:          defaultLogNode.MaxAge,
			RotationTime:    defaultLogNode.RotationTime,
			RotationSize:    defaultLogNode.RotationSize,
			LogInConsole:    defaultLogNode.LogInConsole,
		},
		EventLog: logger.LogNodeConfig{
			LogLevelDefault: defaultLogNode.LogLevelDefault,
			FilePath:        defaultLogNode.FilePath,
			MaxAge:          defaultLogNode.MaxAge,
			RotationTime:    defaultLogNode.RotationTime,
			RotationSize:    defaultLogNode.RotationSize,
			LogInConsole:    defaultLogNode.LogInConsole,
		},
	}
	logger.SetLogConfig(config)
	var chainId = viper.GetString(`chain.ID`)
	log = logger.GetLoggerByChain("enclave-server", chainId)

	log.Debug("log init success")
}

func GetLogger() *logger.CMLogger {
	return log
}
