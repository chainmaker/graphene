/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package service

import (
	"errors"

	info "chainmaker.org/chainmaker/chainmaker-graphene/common/tee"
	"chainmaker.org/chainmaker/chainmaker-graphene/internal/vm-wasm/logger"
	"chainmaker.org/chainmaker/chainmaker-graphene/loggers"
	v2c "chainmaker.org/chainmaker/common/v2/crypto"
	x509v2 "chainmaker.org/chainmaker/common/v2/crypto/x509"
)

var (
	//签名公私钥缓存
	SecreteKeyCache v2c.PrivateKey
	PublickeyCache  v2c.PublicKey

	//签名公私钥缓存
	SecretkeyExtCache v2c.PrivateKey
	PublickeyExtCache []byte
	Report            []byte
	Cert              *x509v2.Certificate
	log               *logger.CMLogger
)

func Init(sign, enc string, isSim bool) ([]byte, []byte, []byte, error) {
	log = loggers.GetLogger()

	s, k := v2c.AsymAlgoMap[sign]
	if !k {
		return nil, nil, nil, errors.New("sign alg was error")
	}
	e, k := v2c.AsymAlgoMap[enc]
	if !k {
		return nil, nil, nil, errors.New("Encrypt alg was error")
	}
	report, csr, err := GetTeeInfo(s, e, isSim)
	if err != nil {
		return nil, nil, nil, err
	}

	pub, err := info.GetEnckey(info.PublickeyExt)
	return report, csr, pub, err
}

func GetTeeInfo(signAlgorithm, cryptoAlgorithm v2c.KeyType, isSim bool) ([]byte, []byte, error) {

	log.Info("start Tee init")
	/*
		step1 : init report
	*/
	report, err := info.InitReport(isSim)
	if err != nil {
		return nil, nil, err
	}
	Report = report
	/*
		step2 : init sign algorithm support SM2,RSA,ECDSA
	*/
	// get ecnlave password
	pwdByte, err := info.GetKeyWord(isSim)
	if err != nil {
		return nil, nil, err
	}

	err = info.GenAlgoKey(info.SecreteKeyFile, info.PublickeyFile, pwdByte, signAlgorithm, false)
	if err != nil {
		return nil, nil, err
	}
	log.Info("generation sign key pair success")

	/*
		step3 : init crypto algorithm support SM2,RSA
	*/
	err = info.GenAlgoKey(info.SecretkeyExt, info.PublickeyExt, pwdByte, cryptoAlgorithm, true)
	if err != nil {
		return nil, nil, err
	}
	log.Info("generation enc key pair success")

	/*
		cache all key in memery
	*/
	PublickeyCache, err = info.GetPubKey(info.PublickeyFile)
	if err != nil {
		return nil, nil, err
	}
	PublickeyExtCache, err = info.GetEnckey(info.PublickeyExt)
	if err != nil {
		return nil, nil, err
	}

	SecreteKeyCache, err = info.GetPrivKey(info.SecreteKeyFile, pwdByte)
	if err != nil {
		return nil, nil, err
	}

	SecretkeyExtCache, err = info.GetPrivKey(info.SecretkeyExt, pwdByte)
	if err != nil {
		return nil, nil, err
	}

	/*
		step4: init CSR
	*/
	csr := []byte{}
	if !info.Exists(info.TeeCsrFile) {
		//  new csr
		if csr, err = info.NewCsr(PublickeyExtCache, SecreteKeyCache); err != nil {
			log.Debug("new encalve csr was error, error = %s", err.Error())
			return nil, nil, err
		}
	}

	//step5: load cert
	/*
		1. fist to check cert can parse
		2. to check cert is true
	*/
	cert, err := info.ParseCert(info.TeeCertFile)
	if err != nil {
		return nil, nil, err
	}
	if cert == nil {
		return nil, nil, errors.New("please load in_teecert.pem")
	}
	if err = info.CheckCert(cert, info.SecreteKeyFile, pwdByte); err != nil {
		if err != nil {
			return nil, nil, err
		}
	}
	log.Debug(" check cert success ")
	Cert = cert

	return report, csr, nil
}
