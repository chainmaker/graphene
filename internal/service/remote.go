/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package service

import (
	"encoding/base64"
	"encoding/json"
	"encoding/pem"

	info "chainmaker.org/chainmaker/chainmaker-graphene/common/tee"
	"chainmaker.org/chainmaker/common/v2/crypto"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"

	"errors"

	"chainmaker.org/chainmaker/pb-go/v2/tee"
)

func TeeRemoteAttestationProve(challenge string, isSim bool) (cert []byte, report []byte, sign []byte, err error) {
	/*
		step1. check challenge
	*/
	log.Info("start tee remote attenstation prove")
	if challenge == "" {
		log.Debug("challenge can not be empty")
		return nil, nil, nil, errors.New("challenge can not be empty")
	}

	/*
		step2. get prive key
	*/
	// get ecnlave pwdword
	pwdByte, err := info.GetKeyWord(isSim)
	if err != nil {
		return nil, nil, nil, err
	}
	priv, err := info.GetPrivKey(info.SecreteKeyFile, pwdByte)
	if err != nil {
		log.Debugf("get prive key was error, error = %s", err)
		return nil, nil, nil, err
	}

	/*
		step3. get cert
	*/
	cert, err = LoadCert(info.TeeCertFile)
	if err != nil {
		log.Debugf("load cert was error, error = %s", err)
		return nil, nil, nil, err
	}

	/*
		step4. get report
	*/
	reportByte, err := info.GetReport(isSim)
	if err != nil {
		log.Debugf("get report was error, error = %s", err)
		return nil, nil, nil, err
	}
	encodeString := base64.StdEncoding.EncodeToString(reportByte)
	report = []byte(encodeString)

	/*
		step5. sign for msg
	*/
	attenation := tee.RemoteAttestationPayload{
		Challenge: challenge,
		Report:    report,
		TeeCert:   cert,
	}

	result, err := json.Marshal(attenation)
	if err != nil {
		return nil, nil, nil, err
	}
	// get the sign key

	hashAlgo, err := bcx509.GetHashFromSignatureAlgorithm(Cert.SignatureAlgorithm)
	if err != nil {
		log.Errorf("invalid algorithm: %v", err.Error())
		return nil, nil, nil, err
	}

	opt := &crypto.SignOpts{
		Hash: hashAlgo,
		UID:  crypto.CRYPTO_DEFAULT_UID,
	}
	sign, err = signPayload(priv, opt, result)
	if err != nil {
		return nil, nil, nil, err
	}

	log.Info("tee remote attenstation prove success")
	return cert, report, sign, nil
}

func LoadCert(path string) ([]byte, error) {

	content, err := info.ReadFile(path)
	if err != nil {
		return nil, err
	}

	block, _ := pem.Decode(content)

	return block.Bytes, nil
}
