/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package service

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"

	wasmer "chainmaker.org/chainmaker/chainmaker-graphene/internal/vm-wasm"
	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/asym/rsa"
	"chainmaker.org/chainmaker/common/v2/crypto/hash"
	"chainmaker.org/chainmaker/common/v2/crypto/sym/aes"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/tee"
	"google.golang.org/grpc"
)

type InvokeParameter struct {
	SymKeyCipher    []byte
	ParamsCipher    []byte
	ContractHash    []byte
	ContractCode    []byte
	ContractVersion []byte
	Params          map[string][]byte
}

// Invoke contract in graphene
func Invoke(ctx context.Context, in *common.TxRequest, conn *grpc.ClientConn) (*tee.EnclaveResponse, error) {
	// get the request parameters
	invokeParameter := extractRequestParameters(in.Payload.Parameters)

	// use aseKey to decrpt request params cipher
	privkeyExt := SecretkeyExtCache
	deckey := privkeyExt.(crypto.DecryptKey)
	requestParamsPlain, err := decryptCipher(invokeParameter.SymKeyCipher, deckey, invokeParameter.ParamsCipher)
	if err != nil {
		return nil, fmt.Errorf("decrypt sym key cipher failed: %w", err)
	}
	// unmarshal invoke payload to params
	var paramMap map[string]interface{}
	err = json.Unmarshal(requestParamsPlain, &paramMap)
	if err != nil {
		return nil, fmt.Errorf("json.Unmarshal requestParamsPlain failed: %w", err)
	}
	invokeParameter.Params = make(map[string][]byte, len(paramMap))
	for k, v := range paramMap {

		if v, ok := v.(string); ok {
			invokeParameter.Params[k] = []byte(v)
		} else {
			return nil, fmt.Errorf("contract method params type is error")
		}

	}

	if err != nil {
		log.Debugf("invalid params payload: %+v", requestParamsPlain)
		return nil, fmt.Errorf("invalid params payload: %w", err)
	}

	// init wasm context
	contractContext, err := wasmer.InitContractContext(
		common.RuntimeType_WASMER,
		invokeParameter.ContractCode,
		invokeParameter.ContractVersion,
		in.Payload,
		in.Sender.Signer,
		vmPoolManager.vm,
		conn,
		false,
		false)
	if err != nil {
		return nil, fmt.Errorf("init wasm context failed: %w", err)
	}

	// invoke contract
	wasmer.BaseParam(invokeParameter.Params)
	contractResp := invokeContract(in.Payload.Method, contractContext.Contract, contractContext.TxContext, contractContext.ByteCode, invokeParameter.Params)

	if contractResp.Code == 1 {
		return nil, fmt.Errorf("invoke contract failed")
	}
	log.Infof("invoke contract %s - %s succeed, result is %s, use %d gas", in.Payload.ContractName, in.Payload.Method, contractResp.Result, contractResp.GasUsed)

	// read report
	report := Report
	reportHash, err := hash.Get(crypto.HASH_TYPE_SM3, report)
	if err != nil {
		return nil, fmt.Errorf("calculate report hash failed: %w", err)
	}

	// fill the response payload
	rwSet := contractContext.TxContext.GetTxRWSet(true)
	payload := &tee.EnclaveResponsePayload{
		ContractResult:  contractResp,
		TxRwset:         rwSet,
		TxRequest:       in,
		ContractName:    contractContext.TxContext.GetContractName(),
		ContractVersion: string(invokeParameter.ContractVersion),
		ContractHash:    string(invokeParameter.ContractHash),
		ReportHash:      string(reportHash),
	}

	enclaveResponsePayloadBytes, err := payload.Marshal()
	if err != nil {
		return nil, fmt.Errorf("marshal enclave response payload failed: %w", err)
	}

	// get the sign key
	privkey := SecreteKeyCache
	hashAlgo, err := bcx509.GetHashFromSignatureAlgorithm(Cert.SignatureAlgorithm)
	if err != nil {
		log.Errorf("invalid algorithm: %v", err.Error())
		return nil, err
	}

	opt := &crypto.SignOpts{
		Hash: hashAlgo,
		UID:  crypto.CRYPTO_DEFAULT_UID,
	}

	switch Cert.SignatureAlgorithm {
	case bcx509.SHA256WithRSAPSS, bcx509.SHA384WithRSAPSS, bcx509.SHA512WithRSAPSS:
		opt.EncodingType = rsa.RSA_PSS
	}

	sign, err := signPayload(privkey, opt, enclaveResponsePayloadBytes)
	if err != nil {
		return nil, err
	}

	enclaveResponse := &tee.EnclaveResponse{
		EnclaveResponsePayload: payload,
		Signature:              sign,
	}
	return enclaveResponse, nil
}

// decrypt textCipher in mixed encryption
func decryptCipher(aeskeyCipher []byte, privkeyExt crypto.DecryptKey, textCipher []byte) ([]byte, error) {
	// Use enclave's private key to decrypt "sym_key_cipher" in payload parameters
	symKeyPlain, err := privkeyExt.Decrypt(aeskeyCipher)
	if err != nil {
		log.Debugf("decrypt symmetric key err is: %w", err.Error())
		return nil, fmt.Errorf("decrypt symmetric key failed: %w", err)
	}

	aeskey := &aes.AESKey{Key: symKeyPlain}
	// Use aseKey to decrpt request params cipher
	requestParamsPlain, err := aeskey.Decrypt(textCipher)
	if err != nil {
		log.Debugf("decrypt request params err is: %w", err.Error())
		return nil, fmt.Errorf("decrypt request params failed: %w", err)
	}
	return requestParamsPlain, nil
}

func invokeContract(method string, contractId *common.Contract, txContext *wasmer.TxContextSimulate, byteCode []byte, parameters map[string][]byte) *common.ContractResult {
	runtime := txContext.GetRuntimeInstance()
	r := runtime.Invoke(contractId, method, byteCode, parameters, txContext, 0)
	return r
}

func extractRequestParameters(kvpair []*common.KeyValuePair) *InvokeParameter {
	invokeParameter := &InvokeParameter{}
	ip := reflect.ValueOf(invokeParameter).Elem()
	for _, v := range kvpair {
		ip.FieldByName(v.Key).Set(reflect.ValueOf(v.Value))
		v.Reset()
	}
	return invokeParameter
}

// test signature verification
func signPayload(key crypto.PrivateKey, opt *crypto.SignOpts, payloadBytes []byte) ([]byte, error) {
	sign, err := key.SignWithOpts(payloadBytes, opt)
	if err != nil {
		log.Debugf("sign payload failed")
		return nil, fmt.Errorf("sign payload failed: %w", err)
	}
	return sign, nil
}

// func compactResult(result common.ContractResult, rwSet common.TxRWSet,
// 	name, version string, codeHash, reportHash, requestBytes, codeHeader []byte) ([]byte, error) {

// 	grapheneResultBuffer := bytes.NewBuffer([]byte{})

// 	// Code
// 	if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, result.Code); err != nil {
// 		return nil, err
// 	}
// 	// Result
// 	if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(result.Result))); err != nil {
// 		return nil, err
// 	}
// 	grapheneResultBuffer.Write(result.Result)
// 	// Gas
// 	if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint64(result.GasUsed)); err != nil {
// 		return nil, err
// 	}
// 	// rsets
// 	if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(rwSet.TxReads))); err != nil {
// 		return nil, err
// 	}
// 	for i := 0; i < len(rwSet.TxReads); i++ {
// 		// Key
// 		if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(rwSet.TxReads[i].Key))); err != nil {
// 			return nil, err
// 		}
// 		grapheneResultBuffer.Write(rwSet.TxReads[i].Key)
// 		// Value
// 		if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(rwSet.TxReads[i].Value))); err != nil {
// 			return nil, err
// 		}
// 		grapheneResultBuffer.Write(rwSet.TxReads[i].Value)
// 		// Version
// 		if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(0)); err != nil {
// 			return nil, err
// 		}
// 		// grapheneResultBuffer.Write([]byte(rwSet.TxReads[i].Version.RefTxId))
// 	}

// 	// wsets
// 	if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(rwSet.TxWrites))); err != nil {
// 		return nil, err
// 	}
// 	for i := 0; i < len(rwSet.TxWrites); i++ {
// 		// Key
// 		if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(rwSet.TxWrites[i].Key))); err != nil {
// 			return nil, err
// 		}
// 		grapheneResultBuffer.Write(rwSet.TxWrites[i].Key)

// 		// Value
// 		if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(rwSet.TxWrites[i].Value))); err != nil {
// 			return nil, err
// 		}
// 		grapheneResultBuffer.Write(rwSet.TxWrites[i].Value)
// 	}

// 	// name
// 	if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(name))); err != nil {
// 		return nil, err
// 	}
// 	grapheneResultBuffer.Write([]byte(name))
// 	// version
// 	if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(version))); err != nil {
// 		return nil, err
// 	}
// 	grapheneResultBuffer.Write([]byte(version))
// 	// code hash
// 	if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(codeHash))); err != nil {
// 		return nil, err
// 	}
// 	grapheneResultBuffer.Write(codeHash)
// 	// report hash
// 	if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(reportHash))); err != nil {
// 		return nil, err
// 	}
// 	grapheneResultBuffer.Write(reportHash)
// 	// user request
// 	if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(requestBytes))); err != nil {
// 		return nil, err
// 	}
// 	grapheneResultBuffer.Write(requestBytes)
// 	// code header
// 	if err := binary.Write(grapheneResultBuffer, binary.LittleEndian, uint32(len(codeHeader))); err != nil {
// 		return nil, err
// 	}
// 	grapheneResultBuffer.Write(codeHeader)

// 	return grapheneResultBuffer.Bytes(), nil
// }
