/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package service

import (
	"context"
	"encoding/json"
	"fmt"

	wasmer "chainmaker.org/chainmaker/chainmaker-graphene/internal/vm-wasm"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/asym/rsa"
	"chainmaker.org/chainmaker/common/v2/crypto/hash"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/tee"
	"google.golang.org/grpc"
)

type VmManager struct {
	chainID string
	vm      *wasmer.VmPoolManager
}

var (
	vmPoolManager *VmManager
	//pool          *sync.Pool
)

func (v *VmManager) initVM() {
	v.vm = wasmer.NewVmPoolManager(v.chainID)
}

func (v *VmManager) GetVm() *VmManager {
	return v
}

func InitVmManager(chainId string) {
	vmPoolManager = &VmManager{chainID: chainId}
	vmPoolManager.initVM()
}

// Invoke contract in graphene
func Deploy(ctx context.Context, in *common.TxRequest, conn *grpc.ClientConn) (*tee.EnclaveResponse, error) {
	// get the request parameters
	invokeParameter := extractRequestParameters(in.Payload.Parameters)

	// use aseKey to decrpt request params cipher
	privkeyExt := SecretkeyExtCache
	deckey := privkeyExt.(crypto.DecryptKey)
	requestParamsPlain, err := decryptCipher(invokeParameter.SymKeyCipher, deckey, invokeParameter.ParamsCipher)
	if err != nil {
		return nil, fmt.Errorf("decrypt sym key cipher failed: %w", err)
	}

	// unmarshal invoke payload to params
	var paramMap map[string]interface{}
	err = json.Unmarshal(requestParamsPlain, &paramMap)
	invokeParameter.Params = make(map[string][]byte, len(paramMap))
	for k, v := range paramMap {
		invokeParameter.Params[k] = []byte(v.(string))
	}
	if err != nil {
		log.Debugf("invalid params payload: %+v", requestParamsPlain)
		return nil, fmt.Errorf("invalid params payload: %w", err)
	}

	// init wasm context
	contractContext, err := wasmer.InitContractContext(common.RuntimeType_WASMER,
		invokeParameter.ContractCode,
		invokeParameter.ContractVersion,
		in.Payload,
		in.Sender.Signer,
		vmPoolManager.vm,
		conn,
		false,
		true)
	if err != nil {
		return nil, fmt.Errorf("init wasm context failed: %w", err)
	}

	// invoke contract
	wasmer.BaseParam(invokeParameter.Params)
	contractResp := invokeContract(in.Payload.Method, contractContext.Contract, contractContext.TxContext, contractContext.ByteCode, invokeParameter.Params)
	if contractResp.Code == 1 {
		return nil, fmt.Errorf("invoke contract failed")
	}
	log.Infof("invoke contract %s - %s succeed, result is %s, use %d gas", in.Payload.ContractName, in.Payload.Method, contractResp.Result, contractResp.GasUsed)

	contractResp.Result = invokeParameter.ContractCode

	// read report
	report := Report
	reportHash, err := hash.Get(crypto.HASH_TYPE_SM3, report)
	if err != nil {
		return nil, fmt.Errorf("calculate report hash failed: %w", err)
	}

	// fill the response payload
	rwSet := contractContext.TxContext.GetTxRWSet(true)
	payload := &tee.EnclaveResponsePayload{
		ContractResult:  contractResp,
		TxRwset:         rwSet,
		TxRequest:       in,
		ContractName:    contractContext.TxContext.GetContractName(),
		ContractVersion: string(invokeParameter.ContractVersion),
		ContractHash:    string(invokeParameter.ContractHash),
		ReportHash:      string(reportHash),
	}
	enclaveResponsePayloadBytes, err := payload.Marshal()
	if err != nil {
		return nil, fmt.Errorf("marshal enclave response payload failed: %w", err)
	}

	// get the sign key
	privkey := SecreteKeyCache
	cert := Cert
	hashAlgo, err := bcx509.GetHashFromSignatureAlgorithm(cert.SignatureAlgorithm)
	if err != nil {
		log.Errorf("invalid algorithm: %v", err.Error())
		return nil, err
	}

	opt := &crypto.SignOpts{
		Hash: hashAlgo,
		UID:  crypto.CRYPTO_DEFAULT_UID,
	}
	switch cert.SignatureAlgorithm {
	case bcx509.SHA256WithRSAPSS, bcx509.SHA384WithRSAPSS, bcx509.SHA512WithRSAPSS:
		opt.EncodingType = rsa.RSA_PSS
	}
	sign, err := signPayload(privkey, opt, enclaveResponsePayloadBytes)
	if err != nil {
		return nil, err
	}

	// p.Response.Signature = sign
	enclaveResponse := &tee.EnclaveResponse{
		EnclaveResponsePayload: payload,
		Signature:              sign,
	}
	return enclaveResponse, nil
}
