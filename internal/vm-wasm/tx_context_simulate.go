/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package wasmer

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"sync"

	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/pb-go/v2/tee"
	"google.golang.org/grpc"

	"chainmaker.org/chainmaker/utils/v2"
	"chainmaker.org/chainmaker/pb-go/v2/config"

	"chainmaker.org/chainmaker/chainmaker-graphene/internal/vm-wasm/logger"
	acPb "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	storePb "chainmaker.org/chainmaker/pb-go/v2/store"
	"chainmaker.org/chainmaker/protocol/v2"
)

// var (
// 	// ContractVersionTest is the contract version for test
// 	ContractVersionTest = "v1.0.0"
// 	ChainId = ""
// 	testOrgId   = "wx-org1.chainmaker.org"
// 	txType      = commonPb.TxType_INVOKE_CONTRACT
// )

// InitContextContext inits a test context for vm
func InitContractContext(runtimeType commonPb.RuntimeType,
	contractCode, version []byte,
	paload *commonPb.Payload,
	signInfo *accesscontrol.Member,
	vmManager *VmPoolManager,
	conn *grpc.ClientConn,
	isSql, isDeploy bool) (*ContractContext, error) {
	log := vmManager.log

	sender := &acPb.Member{
		OrgId:      signInfo.OrgId,
		MemberInfo: signInfo.MemberInfo,
	}

	contractId := commonPb.Contract{
		Name:        paload.ContractName,
		Version:     string(version),
		RuntimeType: runtimeType,
		Creator:     sender,
	}

	runtimeInstance, err := vmManager.NewRuntimeInstance(&contractId, contractCode)
	if err != nil {
		log.Error("new runtime instance error is:", err)
		return nil, err
	}
	if len(paload.TxId) == 0 {
		err = fmt.Errorf("txId length was error :%d", len(paload.TxId))
		log.Error("new contract context error is:", err)
		return nil, err
	}

	txContext := &TxContextSimulate{
		lock:    &sync.Mutex{},
		lock2:   &sync.Mutex{},
		ChainId: paload.ChainId,
		Log:     log,
		//ChainConf:       chainConf,
		isSql:           isSql,
		hisResult:       make([]*callContractResult, 0),
		creator:         sender,
		sender:          sender,
		cacheMap:        make(map[string][]byte),
		kvRowCache:      make(map[int32]protocol.StateIterator),
		runtimeInstance: runtimeInstance,
		conn:            conn,
		readSet:         make(map[string][]byte),
		writeSet:        make(map[string][]byte),
		contractName:    paload.ContractName,
		TxId:            paload.TxId,
	}
	if isDeploy {
		data, _ := contractId.Marshal()
		key := utils.GetContractDbKey(contractId.Name)
		if err := txContext.Put(syscontract.SystemContract_CONTRACT_MANAGE.String(), key, data); err != nil {
			log.Debug("context put key: ", key, "value: ", data, "failed")
			return nil, err
		}
		byteCodeKey := utils.GetContractByteCodeDbKey(contractId.Name)
		if err := txContext.Put(syscontract.SystemContract_CONTRACT_MANAGE.String(), byteCodeKey, contractCode); err != nil {
			log.Debug("context put bytecode failed")
			return nil, err
		}
	}

	return &ContractContext{
		TxContext: txContext,
		Contract:  &contractId,
		ByteCode:  contractCode,
		Id:        paload.TxId,
	}, nil
}

type ContractContext struct {
	Contract  *commonPb.Contract
	TxContext *TxContextSimulate
	ByteCode  []byte
	Id        string
}

type TxContextSimulate struct {
	lock            *sync.Mutex
	lock2           *sync.Mutex
	gasUsed         uint64 // only for callContract
	currentDepth    int
	currentResult   []byte
	hisResult       []*callContractResult
	sender          *acPb.Member
	creator         *acPb.Member
	cacheMap        map[string][]byte
	kvRowCache      map[int32]protocol.StateIterator
	ChainId         string
	Log             *logger.CMLogger
	ChainConf       protocol.ChainConf // chain config
	runtimeInstance *RuntimeInstance
	isSql           bool
	conn            *grpc.ClientConn
	readSet         map[string][]byte
	writeSet        map[string][]byte
	contractName    string
	TxId            string
}

func (s *TxContextSimulate) GetContractByName(name string) (*commonPb.Contract, error) {
	panic("implement me")
}

func (s *TxContextSimulate) GetContractBytecode(name string) ([]byte, error) {
	panic("implement me")
}

func (s *TxContextSimulate) GetRuntimeInstance() *RuntimeInstance {
	return s.runtimeInstance
}

func (s *TxContextSimulate) GetBlockVersion() uint32 {
	return protocol.DefaultBlockVersion
}

func (s *TxContextSimulate) GetContractName() string {
	return s.contractName
}

func (s *TxContextSimulate) PutRecord(contractName string, value []byte, sqlType protocol.SqlType) {
	panic("implement me")
}
func (s *TxContextSimulate) SetStateKvHandle(index int32, rows protocol.StateIterator) {
	s.lock2.Lock()
	defer s.lock2.Unlock()
	s.kvRowCache[index] = rows
}

func (s *TxContextSimulate) GetStateKvHandle(index int32) (protocol.StateIterator, bool) {
	s.lock2.Lock()
	defer s.lock2.Unlock()
	data, ok := s.kvRowCache[index]
	return data, ok
}

func (s *TxContextSimulate) GetBlockProposer() *acPb.Member {
	panic("implement me")
}

func (s *TxContextSimulate) SetStateSqlHandle(i int32, rows protocol.SqlRows) {
	panic("implement me")
}

func (s *TxContextSimulate) GetStateSqlHandle(i int32) (protocol.SqlRows, bool) {
	panic("implement me")
}

type callContractResult struct {
	contractName string
	method       string
	param        map[string][]byte
	deep         int
	gasUsed      uint64
	result       []byte
}

func constructStateKey(contractName string, key []byte) []byte {
	return append(append([]byte(contractName), contractStoreSeparator), key...)
}
func (s *TxContextSimulate) Get(name string, key []byte) ([]byte, error) {
	s.lock.Lock()
	defer s.lock.Unlock()
	key = constructStateKey(name, key)
	k := string(key)
	if val, ok := s.writeSet[k]; ok {
		return val, nil
	} else if val, ok := s.readSet[k]; ok {
		return val, nil
	} else {
		client := tee.NewEnclaveOutCallServerClient(s.conn)
		contractResult, err := client.OutCallGet(context.Background(), &tee.OutCallGetRequest{ContractName: name, Key: k})
		if err != nil {
			return nil, err
		}
		s.readSet[k] = contractResult.Result
		return contractResult.Result, nil
	}
}

const contractStoreSeparator = '#'

func (s *TxContextSimulate) Put(name string, key []byte, value []byte) error {
	key = constructStateKey(name, key)
	k := string(key)
	s.writeSet[k] = value
	return nil
}

func (s *TxContextSimulate) Select(name string, startKey []byte, limit []byte) (protocol.StateIterator, error) {
	return nil, errors.New("Don't support iterator")
}

func (s *TxContextSimulate) Del(name string, key []byte) error {
	key = constructStateKey(name, key)
	k := string(key)
	s.writeSet[k] = nil
	return nil
}

func (s *TxContextSimulate) CallContract(contract *commonPb.Contract, method string, byteCode []byte,
	parameter map[string][]byte, gasUsed uint64, refTxType commonPb.TxType) (*commonPb.ContractResult,
	commonPb.TxStatusCode) {
	s.gasUsed = gasUsed
	s.currentDepth = s.currentDepth + 1
	if s.currentDepth > protocol.CallContractDepth {
		contractResult := &commonPb.ContractResult{
			Code:    uint32(1),
			Result:  nil,
			Message: fmt.Sprintf("CallContract too deep %d", s.currentDepth),
		}
		return contractResult, commonPb.TxStatusCode_CONTRACT_TOO_DEEP_FAILED
	}
	if s.gasUsed > protocol.GasLimit {
		contractResult := &commonPb.ContractResult{
			Code:    uint32(1),
			Result:  nil,
			Message: fmt.Sprintf("There is not enough gas, gasUsed %d GasLimit %d ", gasUsed, int64(protocol.GasLimit)),
		}
		return contractResult, commonPb.TxStatusCode_CONTRACT_FAIL
	}
	if len(byteCode) == 0 {
		dbByteCode, err := utils.GetContractBytecode(s.Get, contract.Name)
		if err != nil {
			return nil, commonPb.TxStatusCode_CONTRACT_FAIL
		}
		byteCode = dbByteCode
	}
	r, code := s.RunContract(contract, method, byteCode, parameter, s, s.gasUsed, refTxType)
	result := callContractResult{
		deep:         s.currentDepth,
		gasUsed:      s.gasUsed,
		result:       r.Result,
		contractName: contract.Name,
		method:       method,
		param:        parameter,
	}
	s.hisResult = append(s.hisResult, &result)
	s.currentResult = r.Result
	s.currentDepth = s.currentDepth - 1
	return r, code
}

func (s *TxContextSimulate) RunContract(contract *commonPb.Contract, method string, byteCode []byte,
	parameters map[string][]byte, txContext protocol.TxSimContext, gasUsed uint64, refTxType commonPb.TxType) (
	*commonPb.ContractResult, commonPb.TxStatusCode) {

	contractResult := &commonPb.ContractResult{
		Code:    uint32(1),
		Result:  nil,
		Message: "",
	}

	contractName := contract.Name
	if contractName == "" {
		contractResult.Message = "contractName not found"
		return contractResult, commonPb.TxStatusCode_INVALID_CONTRACT_PARAMETER_CONTRACT_NAME
	}

	if parameters == nil {
		parameters = make(map[string][]byte)
	}

	if len(contract.Version) == 0 {
		var err error
		contract, err = utils.GetContractByName(txContext.Get, contractName)
		if err != nil {
			contractResult.Message = fmt.Sprintf("query contract[%s] error", contractName)
			return contractResult, commonPb.TxStatusCode_INVALID_CONTRACT_PARAMETER_CONTRACT_NAME
		}
	}
	if !isUserContract(refTxType) {
		contractResult.Message = fmt.Sprintf("bad contract call %s, transaction type %s", contractName, refTxType)
		return contractResult, commonPb.TxStatusCode_INVALID_CONTRACT_TRANSACTION_TYPE
	}

	return s.invokeUserContract(contract, method, parameters, txContext, byteCode, gasUsed)

}

func (s *TxContextSimulate) invokeUserContract(contract *commonPb.Contract, method string,
	parameters map[string][]byte, txContext protocol.TxSimContext, byteCode []byte,
	gasUsed uint64) (*commonPb.ContractResult, commonPb.TxStatusCode) {
	contractResult := &commonPb.ContractResult{Code: uint32(1)}
	txId := txContext.GetTx().Payload.TxId
	txType := txContext.GetTx().Payload.TxType
	runtimeType := contract.RuntimeType
	var err error
	var runtimeInstance RuntimeInstance

	switch runtimeType {
	case commonPb.RuntimeType_WASMER:
		runtimeInstance = *s.runtimeInstance
	default:
		contractResult.Message = fmt.Sprintf("no such vm runtime %q", runtimeType)
		return contractResult, commonPb.TxStatusCode_INVALID_CONTRACT_PARAMETER_RUNTIME_TYPE
	}

	//sender := txContext.GetSender()
	creator := contract.Creator

	if creator == nil {
		contractResult.Message = fmt.Sprintf("creator is empty for contract:%s", contract.Name)
		return contractResult, commonPb.TxStatusCode_GET_CREATOR_FAILED
	}

	parameters[protocol.ContractTxIdParam] = []byte(txId)
	parameters[protocol.ContractBlockHeightParam] = []byte(strconv.FormatUint(txContext.GetBlockHeight(), 10))

	// calc the gas used by byte code
	// gasUsed := uint64(GasPerByte * len(byteCode))

	s.Log.Debugf("invoke vm, tx id:%s, tx type:%+v, contractId:%+v, method:%+v, runtime type:%+v, "+
		"byte code len:%+v, params:%+v", txId, txType, contract, method, runtimeType, len(byteCode), len(parameters))

	// begin save point for sql
	var dbTransaction protocol.SqlDBTransaction
	if s.isSql && txType != commonPb.TxType_QUERY_CONTRACT {
		txKey := commonPb.GetTxKeyWith(txContext.GetBlockProposer().MemberInfo, txContext.GetBlockHeight())
		dbTransaction, err = txContext.GetBlockchainStore().GetDbTransaction(txKey)
		if err != nil {
			contractResult.Message = fmt.Sprintf("get db transaction from [%s] error %+v", txKey, err)
			return contractResult, commonPb.TxStatusCode_INTERNAL_ERROR
		}
		err = dbTransaction.BeginDbSavePoint(txId)
		if err != nil {
			s.Log.Warn("[%s] begin db save point error, %s", txId, err.Error())
		}
		//txContext.Put(contractId.Name, []byte("target"), []byte("mysql")) // for dag
	}

	runtimeContractResult := runtimeInstance.Invoke(contract, method, byteCode, parameters, txContext, gasUsed)
	if runtimeContractResult.Code == 0 {
		return runtimeContractResult, commonPb.TxStatusCode_SUCCESS
	}

	if s.isSql && txType != commonPb.TxType_QUERY_CONTRACT {
		err = dbTransaction.RollbackDbSavePoint(txId)
		if err != nil {
			s.Log.Warn("[%s] rollback db save point error, %s", txId, err.Error())
		}
	}
	return runtimeContractResult, commonPb.TxStatusCode_CONTRACT_FAIL
}

func isUserContract(refTxType commonPb.TxType) bool {
	switch refTxType {
	case
		commonPb.TxType_INVOKE_CONTRACT,
		commonPb.TxType_QUERY_CONTRACT:
		return true
	default:
		return false
	}
}

func (s *TxContextSimulate) GetCurrentResult() []byte {
	return s.currentResult
}

func (s *TxContextSimulate) GetTx() *commonPb.Transaction {
	return &commonPb.Transaction{
		Payload: &commonPb.Payload{
			ChainId:        s.ChainId,
			TxType:         commonPb.TxType(s.runtimeInstance.pool.contractId.RuntimeType),
			TxId:           s.TxId,
			Timestamp:      0,
			ExpirationTime: 0,
		},
		Result: nil,
	}
}

func (*TxContextSimulate) GetBlockHeight() uint64 {
	return 0
}
func (s *TxContextSimulate) GetTxResult() *commonPb.Result {
	panic("implement me")
}

func (s *TxContextSimulate) SetTxResult(txResult *commonPb.Result) {
	panic("implement me")
}

func (s *TxContextSimulate) GetTxRWSet(runVmSuccess bool) *commonPb.TxRWSet {
	rwSet := &commonPb.TxRWSet{TxId: "txId"}
	for k, v := range s.readSet {
		rwSet.TxReads = append(rwSet.TxReads,
			&commonPb.TxRead{
				Key:          []byte(k),
				Value:        v,
				ContractName: s.contractName,
				Version:      nil,
			})
	}
	for k, v := range s.writeSet {
		rwSet.TxWrites = append(rwSet.TxWrites,
			&commonPb.TxWrite{
				Key:          []byte(k),
				Value:        v,
				ContractName: s.contractName,
			})
	}
	return rwSet
}

func (s *TxContextSimulate) GetCreator(namespace string) *acPb.Member {
	return s.creator
}

func (s *TxContextSimulate) GetSender() *acPb.Member {
	return s.sender
}

func (*TxContextSimulate) GetBlockchainStore() protocol.BlockchainStore {
	panic("implement me")
}
func (*TxContextSimulate) GetAccessControl() (protocol.AccessControlProvider, error) {
	panic("implement me")
}

func (s *TxContextSimulate) GetChainNodesInfoProvider() (protocol.ChainNodesInfoProvider, error) {
	panic("implement me")
}

func (*TxContextSimulate) GetTxExecSeq() int {
	panic("implement me")
}

func (*TxContextSimulate) SetTxExecSeq(i int) {
	panic("implement me")
}

func (s *TxContextSimulate) GetDepth() int {
	return s.currentDepth
}

func BaseParam(parameters map[string][]byte) {
	parameters[protocol.ContractTxIdParam] = []byte("TX_ID")
	parameters[protocol.ContractCreatorOrgIdParam] = []byte("CREATOR_ORG_ID")
	parameters[protocol.ContractCreatorRoleParam] = []byte("CREATOR_ROLE")
	parameters[protocol.ContractCreatorPkParam] = []byte("CREATOR_PK")
	parameters[protocol.ContractSenderOrgIdParam] = []byte("SENDER_ORG_ID")
	parameters[protocol.ContractSenderRoleParam] = []byte("SENDER_ROLE")
	parameters[protocol.ContractSenderPkParam] = []byte("SENDER_PK")
	parameters[protocol.ContractBlockHeightParam] = []byte("111")
}

type mockBlockchainStore struct {
}

func (m mockBlockchainStore) GetMemberExtraData(member *acPb.Member) (*acPb.MemberExtraData, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetContractByName(name string) (*commonPb.Contract, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetContractBytecode(name string) ([]byte, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetHeightByHash(blockHash []byte) (uint64, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetBlockHeaderByHeight(height uint64) (*commonPb.BlockHeader, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetTxHeight(txId string) (uint64, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetArchivedPivot() uint64 {
	panic("implement me")
}

func (m mockBlockchainStore) ArchiveBlock(archiveHeight uint64) error {
	panic("implement me")
}

func (m mockBlockchainStore) RestoreBlocks(serializedBlocks [][]byte) error {
	panic("implement me")
}

func (m mockBlockchainStore) GetLastChainConfig() (*config.ChainConfig, error) {
	panic("implement me")
}

func (m mockBlockchainStore) SelectObject(contractName string, startKey []byte, limit []byte) (
	protocol.StateIterator, error) {
	panic("implement me")
}

func (m mockBlockchainStore) QuerySingle(contractName, sql string, values ...interface{}) (protocol.SqlRow, error) {
	panic("implement me")
}

func (m mockBlockchainStore) QueryMulti(contractName, sql string, values ...interface{}) (protocol.SqlRows, error) {
	panic("implement me")
}

func (m mockBlockchainStore) ExecDdlSql(contractName, sql string, s string) error {
	panic("implement me")
}

func (m mockBlockchainStore) BeginDbTransaction(txName string) (protocol.SqlDBTransaction, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetDbTransaction(txName string) (protocol.SqlDBTransaction, error) {
	panic("implement me")
}

func (m mockBlockchainStore) CommitDbTransaction(txName string) error {
	panic("implement me")
}

func (m mockBlockchainStore) RollbackDbTransaction(txName string) error {
	panic("implement me")
}

func (m mockBlockchainStore) InitGenesis(genesisBlock *storePb.BlockWithRWSet) error {
	panic("implement me")
}

func (m mockBlockchainStore) PutBlock(block *commonPb.Block, txRWSets []*commonPb.TxRWSet) error {
	panic("implement me")
}

func (m mockBlockchainStore) GetHistoryForKey(contractName string, key []byte) (protocol.KeyHistoryIterator, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetAccountTxHistory(accountId []byte) (protocol.TxHistoryIterator, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetContractTxHistory(contractName string) (protocol.TxHistoryIterator, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetBlockByHash(blockHash []byte) (*commonPb.Block, error) {
	panic("implement me")
}

func (m mockBlockchainStore) BlockExists(blockHash []byte) (bool, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetBlock(height uint64) (*commonPb.Block, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetLastConfigBlock() (*commonPb.Block, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetBlockByTx(txId string) (*commonPb.Block, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetBlockWithRWSets(height uint64) (*storePb.BlockWithRWSet, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetTx(txId string) (*commonPb.Transaction, error) {
	panic("implement me")
}

func (m mockBlockchainStore) TxExists(txId string) (bool, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetTxConfirmedTime(txId string) (int64, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetLastBlock() (*commonPb.Block, error) {
	return &commonPb.Block{
		Header: &commonPb.BlockHeader{
			ChainId:        "",
			BlockHeight:    0,
			PreBlockHash:   nil,
			BlockHash:      nil,
			PreConfHeight:  0,
			BlockVersion:   0,
			DagHash:        nil,
			RwSetRoot:      nil,
			TxRoot:         nil,
			BlockTimestamp: 0,
			Proposer:       nil,
			ConsensusArgs:  nil,
			TxCount:        0,
			Signature:      nil,
		},
		Dag:            nil,
		Txs:            nil,
		AdditionalData: nil,
	}, nil
}

func (m mockBlockchainStore) ReadObject(contractName string, key []byte) ([]byte, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetTxRWSet(txId string) (*commonPb.TxRWSet, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetTxRWSetsByHeight(height uint64) ([]*commonPb.TxRWSet, error) {
	panic("implement me")
}

func (m mockBlockchainStore) GetDBHandle(dbName string) protocol.DBHandle {
	panic("implement me")
}

func (m mockBlockchainStore) Close() error {
	panic("implement me")
}

// type kvi struct {
// 	iter         protocol.Iterator
// 	contractName string
// }

// func (i *kvi) Next() bool {
// 	return i.iter.Next()
// }
// func (i *kvi) Value() (*storePb.KV, error) {
// 	err := i.iter.Error()
// 	if err != nil {
// 		return nil, err
// 	}
// 	return &storePb.KV{
// 		ContractName: i.contractName,
// 		Key:          parseStateKey(i.iter.Key(), i.contractName),
// 		Value:        i.iter.Value(),
// 	}, nil
// }

// func (i *kvi) Release() {
// 	i.iter.Release()
// }

// // parseStateKey corresponding to the constructStateKey(),  delete contract name from leveldb key
// func parseStateKey(key []byte, contractName string) []byte {
// 	return key[len(contractName)+1:]
// }

//
//func getFullCertMember(sender *acPb.Member, txContext protocol.TxSimContext) (*acPb.Member, commonPb.TxStatusCode) {
//	// If the certificate in the transaction is hash, the original certificate is retrieved
//	if sender.MemberType == acPb.MemberType_CERT_HASH {
//		memberInfoHex := hex.EncodeToString(sender.MemberInfo)
//		var fullCertMemberInfo []byte
//		var err error
//		if fullCertMemberInfo, err = txContext.Get(syscontract.SystemContract_CERT_MANAGE.String(),
//			[]byte(memberInfoHex)); err != nil {
//			return nil, commonPb.TxStatusCode_GET_SENDER_CERT_FAILED
//		}
//		sender = &acPb.Member{
//			OrgId:      sender.OrgId,
//			MemberInfo: fullCertMemberInfo,
//			MemberType: acPb.MemberType_CERT,
//		}
//	}
//	return sender, commonPb.TxStatusCode_SUCCESS
//}
