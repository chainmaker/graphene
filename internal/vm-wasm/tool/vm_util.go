/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package tool

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"time"
)

// BytesToInt transform bytes to int32, little endian
func BytesToInt(b []byte) int32 {
	bytesBuffer := bytes.NewBuffer(b)

	var x int32
	if err := binary.Read(bytesBuffer, binary.LittleEndian, &x); err != nil {
		panic(fmt.Sprintf("binary.Read() error: %v", err))
	}

	return int32(x)
}

// BytesToInt transform bytes to int64, little endian
func BytesToInt64(b []byte) int64 {
	bytesBuffer := bytes.NewBuffer(b)
	var x int64
	if err := binary.Read(bytesBuffer, binary.LittleEndian, &x); err != nil {
		panic(fmt.Sprintf("binary.Read() error: %v", err))
	}
	return int64(x)
}

// IntToBytes transform int32 to bytes, little endian
func IntToBytes(x int32) []byte {
	bytesBuffer := bytes.NewBuffer([]byte{})
	if err := binary.Write(bytesBuffer, binary.LittleEndian, x); err != nil {
		panic("binary.Write() error.")
	}
	return bytesBuffer.Bytes()
}

// Int64ToBytes transform int64 to bytes, little endian
func Int64ToBytes(x int64) []byte {
	bytesBuffer := bytes.NewBuffer([]byte{})
	if err := binary.Write(bytesBuffer, binary.LittleEndian, x); err != nil {
		panic("binary.Write() error.")
	}
	return bytesBuffer.Bytes()
}

func CurrentTimeMillisSeconds() int64 {
	return time.Now().UnixNano() / 1e6
}
