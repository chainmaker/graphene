# ChainMaker-Graphene SGX 使用教程



[TOC]



## 1.0.说明

### 1.1.Gramine(原Graphene)

- Graphene 是官方原有的项目名称后来更改为Gramine，下文都改成Gramine
- Gramine 使用v1.1，仓库地址：https://github.com/gramineproject/gramine
- 1.1之前的版本对Golang的支持不友好，不能使用
- 检查设备是否支持sgx
    - 如果设备不支持sgx可以使用simulation mode 运行


## 2.0. 环境配置安装

### 2.1.系统配置

- 系统：linux  (不支持windows,本文教程使用ubuntu20.04)
- 内核： 5.11+（推荐使用5.11以上版本）
- 内存： 8G+

### 2.2.Gramine 环境配置

安装教程：https://gramine.readthedocs.io/en/latest/quickstart.html



## 3.0.Enclave-Server 编译

### 3.1.CA准备

- 准备CA
    - 在tee目录下的info_test.go文件中可以生成第三方CA仅做测试使用
    - 或自行准备第三方CA

### 3.2.准备 enclave-server.manifest.template文件

``` json

loader.preload = "file:{{ gramine.libos }}"
libos.entrypoint = "{{ entrypoint }}"
loader.log_level = "{{ log_level }}"

loader.env.LD_LIBRARY_PATH = "/lib:{{ arch_libdir }}:/usr/lib:/usr{{ arch_libdir }}"

loader.pal_internal_mem_size = "1G"
loader.insecure__use_cmdline_argv = true

sys.enable_sigterm_injection = true

fs.mount.lib.type = "chroot"
fs.mount.lib.path = "/lib"
fs.mount.lib.uri = "file:{{ gramine.runtimedir() }}"

fs.mount.lib2.type = "chroot"
fs.mount.lib2.path = "{{ arch_libdir }}"
fs.mount.lib2.uri = "file:{{ arch_libdir }}"


fs.mount.tmp.type = "chroot"
fs.mount.tmp.path = "/tmp"
fs.mount.tmp.uri = "file:/tmp"

# fs.mount.libos.path 
# fs.mount.libos.uri
# set Absolute Path
fs.mount.libos.type = "chroot"
fs.mount.libos.path = "/home/XXX/code-w/chainmaker-graphene/"
fs.mount.libos.uri = "file:/home/XXX/code-w/chainmaker-graphene/"

sgx.nonpie_binary = true
sgx.enclave_size = "16G"  #根据自己的机器配置型改，建议最小分配8G内存
sys.stack.size = "128M"
sgx.thread_num = 256      #根据机器配置优化调整

sgx.trusted_files = [
    "file:{{ entrypoint }}",
    "file:{{ gramine.runtimedir() }}/",
    "file:{{ arch_libdir }}/",
    "file:/usr{{ arch_libdir }}/",
    "file:/etc/mime.types",
    "file:/etc/default/apport",
]


sgx.allowed_files = [
    "file:/etc/nsswitch.conf",
    "file:/etc/ethers",
    "file:/etc/hosts",
    "file:/etc/group",
    "file:/etc/passwd",
    "file:/etc/gai.conf",
    "file:/etc/host.conf",
    "file:/etc/resolv.conf",
    "file:./configs/",
    "file:/tmp",
    "file:"/home/XXX/code-w/chainmaker-graphene/",  # 设置绝对路径
    "file:./logs",
]
```

其他字段可自行参考https://gramine.readthedocs.io/en/latest/manifest-syntax.html 进行设置

### 3.2.build

- 生成 enclave-key.pem

```
openssl genrsa -3 -out /home/XXX/chainmaker-graphene/enclave-key.pem 3072 //替换自己的目录
```

- 执行 build.sh 文件编译

```shell
  sudo ./build.sh SIM //模拟模式下运行
  sudo ./build.sh SGX=1 DEBUG=1//硬件模式下运行（需要cpu支持）
  DEBUG=1 可选模式
```



## 4.0.部署运行

### 4.1.模拟模式

- gramine-direct   ./enclave-server -module=1

### 4.2.硬件模式

- 运行 gramine-sgx  ./enclave-server
- 第一次部署 初始化完成后需要将csr文件
- 使用得到的CSR文件在第三方CA处申请签发TEE证书
- 将通过步骤4签发的TEE证书以PEM格式存于文件(in_teecert.pem)并放在chainmaker-graphene/configs目录下
- 重新运行程序后会自动校验和加载TEE证书

**注： enclave启动成功，比如 netstat -ntlp  查询有 ./loader的占用标识enclave启动成功**

### 4.3.信息上链

- 使用以下[CMC](https://docs.chainmaker.org.cn/dev/命令行工具.html)命令调用系统合约将得到的report信息上链

```shell
cmc tee upload_report \
--sdk-conf-path={./testdata/sdk_config.yml(SDK配置文件路径)} \
--report={report路径} \
--admin-key-file-paths={key路径} \
--admin-crt-file-paths={证书路径}
```

**注：若Enclave代码版本发生变化，需要再次执行步骤2将更新过的report信息重新上链**

- 将第三方CA的签名跟证书上链

```shell
cmc tee upload_ca_cert \
--sdk-conf-path={./testdata/sdk_config.yml(SDK配置文件路径)} \
--ca_cert={根证书地址} \
--admin-key-file-paths={key路径} \
--admin-crt-file-paths={证书路径}
```

### 4.4.启动网关

- sgx/gateway/config.yml配置文件里面的路径需要修改，/sgx/gateway/cert需要替换

```yaml
    capaths:
      - /home/XXX/work/sgx/gateway/cert/ca                          //替换
    chainid: chain1
    conncnt: 1
    nodeaddr: 127.0.0.1:12301
    orgid: wx-org1.chainmaker.org
    tlshostname: consensus1.tls.wx-org1.chainmaker.org
    usercrtpath: /home/XXX/work/sgx/gateway/cert/client1.tls.crt     //替换
    userkeypath: /home/XXX/work/sgx/gateway/cert/client1.tls.key     //替换
    validtime: 100
```

- 启动隐私网关

```shell
cd ../gateway
go build maim.go
./main start
```

#### 4.4.1.远程证明接口

- 接口地址：http://x.x.x.x:port/private/remote_attestation，其中x.x.x.x:port为服务地址，用户可以在配置里指定。

#### 4.4.2.部署隐私合约

- 接口地址：http://x.x.x.x:port/private/deploy，其中x.x.x.x:port为服务地址，用户可以在配置里指定。

#### 4.4.2.执行远程计算接口

- 接口地址：http://x.x.x.x:port/private/compute，其中x.x.x.x:port为服务地址，用户可以在配置里指定。



## 5.0.示例参考

- 请参考sgx项目下gateway/service/compute_test.go
- 压测示例请参考gateway/service/tools/main.go

```shell
go run main.go 1 1 0
第一个参数：表示开启协程数量
第二个参数：表示循环多少次
第三个参数：表示第几个任务
```



