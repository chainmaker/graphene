module chainmaker.org/chainmaker/chainmaker-graphene

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.1.0
	chainmaker.org/chainmaker/pb-go/v2 v2.1.0
	chainmaker.org/chainmaker/protocol/v2 v2.1.0
	chainmaker.org/chainmaker/utils/v2 v2.1.0
	github.com/flyaways/pool v1.0.1
	github.com/go-playground/assert/v2 v2.0.1
	github.com/spf13/viper v1.9.0
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.17.0
	google.golang.org/grpc v1.40.0
)
