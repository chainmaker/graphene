 /*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
 */


#include "tool.h"

#define SGX_KR_FILE_PATH "../../configs/sgxkeyrequest.dat"

int getReport(unsigned char* user_report_data_str, size_t data_len, sgx_report_t* report){
	ssize_t bytes;
	sgx_target_info_t target_info;
	bytes = rw_file_f("/dev/attestation/my_target_info", (char*)&target_info, sizeof(target_info),false);
	if (bytes != sizeof(target_info)) {
        //error is already printed by rw_file_f() 
		return 1;
	}
	
	bytes = rw_file_f("/dev/attestation/target_info", (char*)&target_info, sizeof(target_info),true);
	if (bytes != sizeof(target_info)) {
        // error is already printed by rw_file_f() 
		return 2;
	}
	 //3. write some custom data to `user_report_data` file 
	if (user_report_data_str && data_len) {
		sgx_report_data_t user_report_data = {0};
		if (sizeof(user_report_data) < data_len) {
			printf("insufficient size of user_report_data");
			return 3;
		}
		memcpy((void*)&user_report_data, (void*)user_report_data_str, data_len);
		bytes = rw_file_f("/dev/attestation/user_report_data", (char*)&user_report_data,
	                      sizeof(user_report_data),true);			
		if (bytes != sizeof(user_report_data)) {
	        // error is already printed by rw_file_f() 
			return 3;
		}
	}
	/* 4. read `report` file */
	bytes = rw_file_f("/dev/attestation/report", (char*)report, sizeof(*report),/*do_write=*/false);
	if (bytes != sizeof(*report)) {
        /* error is already printed by rw_file_f() */
		return 4;
	}
	return 0;
}

ssize_t rw_file_f(const char* path, char* buf, size_t bytes, bool do_write){
	ssize_t rv = 0;
	ssize_t ret = 0;
	int fd = open(path, do_write ? O_WRONLY : O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "opening %s failed\n", path);
		return fd;
	}
	while (bytes > rv) {
		if (do_write)
			ret = write(fd, buf + rv, bytes - rv);
		else
			ret = read(fd, buf + rv, bytes - rv);
		if (ret > 0) {
			rv += ret;
		} else if (ret == 0) {
			// end of file 
			if (rv == 0)
				fprintf(stderr, "%s failed: unexpected end of file\n", do_write ? "write" : "read");
			break;
		} else {
			if (ret < 0 && (errno == EAGAIN || errno == EINTR)) {
				continue;
			} else {
				fprintf(stderr, "%s failed: %s\n", do_write ? "write" : "read", strerror(errno));
				goto out;
			}
		}
	}
out:
	if (ret < 0) {
		//error path 
		close(fd);
		return ret;
	}
	ret = close(fd);
	if (ret < 0) {
		fprintf(stderr, "closing %s failed\n", path);
		return ret;
	}
	return rv;
}


// void setStruct(void **ppDetectInfo)
// {
//     sgx_report_t *pReportInfo = (sgx_report_t *)malloc(sizeof(sgx_report_t));
//     memset(pReportInfo, 0 , sizeof(pReportInfo));

//     fprintf(stdout, "A pDetectInfo address : %p\n", pReportInfo);
//     *ppDetectInfo = pReportInfo;
// }


int getKeyWord(unsigned char *keyWord, bool isInit){
	int i;
	int retValue = 0;
	int ret = 0;
	FILE *krfile;
	sgx_report_t report;
	__sgx_mem_aligned sgx_key_request_t key_request;
	ret = getReport(NULL, 0, &report);
	if(ret != 0){
        //error
		printf("getKeyWord_getReport error %d\n",ret);
		retValue = 1;
		goto freeParameter;
	}
	if (!isInit) {
		krfile = fopen(SGX_KR_FILE_PATH, "rb");
		if (!krfile) {
			printf("getKeyWord_fopen error\n");
			return 2;
		}
		if (1 != fread(&key_request, sizeof(key_request), 1, krfile)) {
			printf("getKeyWord_fread error\n");
			return 3;			
		}
		fclose(krfile);
	} else {
		memset(&key_request, 0, sizeof(key_request));
	   	key_request.key_name = SEAL_KEY;
	   	key_request.key_policy = SGX_KEYPOLICY_MRSIGNER | SGX_KEYPOLICY_MRENCLAVE;
	   	if (report.body.attributes.flags & SGX_FLAGS_KSS) {
	   		key_request.key_policy |= KEY_POLICY_KSS;
	   	}
		key_request.isv_svn = report.body.isv_svn;
		key_request.cpu_svn = report.body.cpu_svn;
		key_request.config_svn = report.body.config_svn;
		for (i = 0; i < SGX_KEYID_SIZE; ++i) {
			key_request.key_id.id[i] = (uint8_t)rdrand();
		}
		key_request.attribute_mask.flags = TSEAL_DEFAULT_FLAGSMASK;
		key_request.attribute_mask.xfrm = 0;
		key_request.misc_mask = TSEAL_DEFAULT_MISCMASK;
		krfile = fopen(SGX_KR_FILE_PATH, "wb");
		if (!krfile) {
			printf("getKeyWord_fopen error\n");
			return 2;
		}
		if (1 != fwrite(&key_request, sizeof(key_request), 1, krfile)) {
			printf("getKeyWord_fread error\n");
			return 3;			
		}
		fclose(krfile);
	}
   	memcpy(&key_request.key_id, &report.key_id, sizeof(key_request.key_id));
	memset(keyWord, 0, 17);
	sgx_getkey(&key_request, (sgx_key_128bit_t*)keyWord);
	keyWord[16] = '\0';
freeParameter:
	return retValue;
}