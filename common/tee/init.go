/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package info

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"time"

	"os"
	"unsafe"

	"chainmaker.org/chainmaker/chainmaker-graphene/internal/vm-wasm/logger"
	"chainmaker.org/chainmaker/chainmaker-graphene/configs"
	"chainmaker.org/chainmaker/chainmaker-graphene/loggers"

	v2c "chainmaker.org/chainmaker/common/v2/crypto"
	as "chainmaker.org/chainmaker/common/v2/crypto/asym"
	sm2 "chainmaker.org/chainmaker/common/v2/crypto/asym/sm2"
	x509v2 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	//"github.com/tjfoc/gmsm/sm2"
)

/*
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include "./sgx_arch.h"
#include "./tool.h"
int getReport(unsigned char* user_report_data_str, size_t data_len, sgx_report_t* report);
int getKeyWord(unsigned char *keyWord, bool isInit);
*/
import "C"

var log *logger.CMLogger
var (
	TeeCsrFile    = ""
	TeeCertFile   = ""
	TeeReportFile = ""

	//签名公私钥
	SecreteKeyFile = ""
	PublickeyFile  = ""

	//加解密公私钥
	SecretkeyExt = ""
	PublickeyExt = ""
	PwdFile      = ""
	Sim          = ""
)

func Init() {
	viperConfig := configs.GetConfig()
	TeeCsrFile = viperConfig.GetString("csr.path")
	TeeCertFile = viperConfig.GetString("cert.path")
	TeeReportFile = viperConfig.GetString("report.path")

	//签名公私钥
	SecreteKeyFile = viperConfig.GetString("crypto.signPrivKey")
	PublickeyFile = viperConfig.GetString("crypto.signPubKey")

	//加解密公私钥
	SecretkeyExt = viperConfig.GetString("crypto.encryptPrivKey")
	PublickeyExt = viperConfig.GetString("crypto.encryptPubKey")

	//pwd file
	PwdFile = viperConfig.GetString("crypto.passwd")
	Sim = viperConfig.GetString("sim.byte")

	// get log config
	log = loggers.GetLogger()
}

func GenAlgoKey(privPath, pubPath string, pwd []byte, keyType v2c.KeyType, isEnc bool) error {
	//check file exist
	if Exists(privPath) && Exists(pubPath) {
		log.Debugf("file was exist privPath=%s, pubPath=%s", privPath, pubPath)
		return nil
	}

	if !isEnc {
		err := genSignKey(privPath, pubPath, pwd, keyType)
		if err != nil {
			log.Debugf("generate sign key pair was error, error = %s", err.Error())
			return err
		}
	} else {
		err := genEncKey(privPath, pubPath, pwd, keyType)
		if err != nil {
			log.Debugf("generate enc key pair was error, error = %s", err.Error())
			return err
		}
	}

	return nil
}

// init report
func InitReport(isSim bool) ([]byte, error) {

	report, err := GetReport(isSim)
	if err != nil {
		log.Debugf("get report failed, error is = %s", err.Error())
		return nil, err
	}
	log.Info("get report success")

	//encode
	encodeString := base64.StdEncoding.EncodeToString(report)
	err = saveReport(encodeString, TeeReportFile)
	if err != nil {
		log.Debugf("save report failed, error is = %s", err.Error())
		return nil, err
	}
	return report, nil
}

func GetReport(isSim bool) ([]byte, error) {
	if isSim {
		return []byte(Sim), nil
	}

	// get enclave report
	var report C.sgx_report_t
	ret := C.getReport(nil, 0, (*C.sgx_report_t)(&report))
	if ret != 0 {
		log.Debugf("get report fail from sgx, ret is = %d", ret)
		return nil, errors.New(" get report fail from sgx")
	}

	//C.report to golang byte
	var pReport unsafe.Pointer = unsafe.Pointer(&report)
	byteReport := C.GoBytes(pReport, C.SGX_REPORT_ACTUAL_SIZE)
	isSim = false
	return byteReport, nil
}

func saveReport(encodeString, reportPath string) error {
	// write report file
	if err := ioutil.WriteFile(reportPath, []byte(encodeString), 0664); err != nil {
		return err
	}
	return nil
}

//for c code struct to golang []byte type
// type SliceMock struct {
// 	addr uintptr
// 	len  int
// 	cap  int
// }

// func byteToStruct(point unsafe.Pointer) *C.sgx_report_t {
// 	// byte back to struct
// 	Len := unsafe.Sizeof(point)
// 	bytes := &SliceMock{
// 		addr: uintptr(point),
// 		cap:  int(Len),
// 		len:  int(Len),
// 	}
// 	data := *(*[]byte)(unsafe.Pointer(bytes))
// 	var reportStruct *C.sgx_report_t = *(**C.sgx_report_t)(unsafe.Pointer(&data))
// 	return reportStruct
// }

func Exists(path string) bool {
	_, err := os.Stat(path) //os.Stat获取文件信息
	if err != nil {
		return os.IsExist(err)
	}
	defer os.Clearenv()
	return true
}

// gen sign Key
func genSignKey(privPath, pubPath string, pwd []byte, key v2c.KeyType) error {

	// private key
	priv, pub, err := as.GenerateKeyPairBytes(key)
	if err != nil {
		return err
	}
	err = genKeyFile(privPath, pubPath, priv, pub, pwd)
	if err != nil {
		return err
	}
	return nil
}

func genEncKey(privPath, pubPath string, pwd []byte, key v2c.KeyType) error {

	// SM2 enc need other
	if key == v2c.SM2 {
		return genSm2EncKey(privPath, pubPath, pwd, key)
	}
	return genEcdsaEncKey(privPath, pubPath, pwd, key)
}
func genEcdsaEncKey(privPath, pubPath string, pwd []byte, key v2c.KeyType) error {
	priv, err := as.GenerateEncKeyPair(key)
	if err != nil {
		return err
	}
	privByte, err := priv.Bytes()
	if err != nil {
		return err
	}

	pubBytes, err := priv.EncryptKey().Bytes()
	if err != nil {
		return err
	}
	err = genKeyFile(privPath, pubPath, privByte, pubBytes, pwd)
	if err != nil {
		return err
	}
	return nil
}

func genSm2EncKey(privPath, pubPath string, pwd []byte, key v2c.KeyType) error {
	privSm2, err := sm2.New(key)
	if err != nil {
		return err
	}
	privByte, err := privSm2.Bytes()
	if err != nil {
		return err
	}
	pubBytes, err := privSm2.PublicKey().Bytes()
	if err != nil {
		return err
	}
	err = genKeyFile(privPath, pubPath, privByte, pubBytes, pwd)
	if err != nil {
		return err
	}
	return nil
}

func genKeyFile(privPath, pubPath string, priv, pub, pwd []byte) error {
	block, err := x509.EncryptPEMBlock(rand.Reader, "PRIVATE KEY", priv, pwd, x509.PEMCipherAES128)
	if err != nil {
		return err
	}

	// to weite privkey to path
	err = WriteFile(block, privPath)
	if err != nil {
		return err
	}

	// to weite pubkey to path
	block = &pem.Block{
		Type:  "PUBLICK KEY",
		Bytes: pub,
	}
	err = WriteFile(block, pubPath)
	if err != nil {
		return err
	}
	return nil
}
func WriteFile(block *pem.Block, path string) error {

	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	err = pem.Encode(file, block)
	if err != nil {
		return err
	}
	return nil
}

func ReadFile(path string) ([]byte, error) {

	//get cert
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	content, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	return content, nil
}

func NewCsr(publickeyExt []byte, priv v2c.PrivateKey) ([]byte, error) {

	subject := pkix.Name{
		Country:      []string{"CN"},
		Organization: []string{"wx.chainmaker.org"},
		CommonName:   "tee.chainmaker.org",
	}

	/*
		[]int{1,2,840,113549,1,9,20}       ->  friendlyName
		[]int{1,2,840,113549,1,9,21}       ->  localKeyID
		[]int{1,2,840,113549,1,12,10,1,1}  ->  keyBag
	*/
	ext1 := addExt([]int{1, 2, 840, 113549, 1, 9, 20}, false, []byte("quote"))
	ext2 := addExt([]int{1, 2, 840, 113549, 1, 9, 21}, false, []byte("123456789"))
	ext3 := addExt([]int{1, 2, 840, 113549, 1, 12, 10, 1, 1}, false, publickeyExt)

	// signatureAlgorithm, err := getSignatureAlgorithm(priv)
	// if err != nil {
	// 	return nil, err
	// }
	signatureAlgorithm := x509.SignatureAlgorithm(0)
	csrTmpl := &x509.CertificateRequest{
		SignatureAlgorithm: signatureAlgorithm,
		ExtraExtensions:    []pkix.Extension{ext1, ext2, ext3},
		Subject:            subject,
	}

	cmCsr, err := x509v2.X509CertCsrToChainMakerCertCsr(csrTmpl)
	if err != nil {
		return nil, err
	}
	csr, err := x509v2.CreateCertificateRequest(rand.Reader, cmCsr, priv.ToStandardKey())
	if err != nil {
		return nil, err
	}

	//save as pem file
	block := pem.Block{
		Type:    "CERTIFICATE REQUEST",
		Headers: nil,
		Bytes:   csr,
	}

	//pem encode write to file
	file, err := os.Create(TeeCsrFile)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	err = pem.Encode(file, &block)
	if err != nil {
		return nil, err
	}

	return csr, nil
}

func addExt(oid []int, critical bool, value []byte) pkix.Extension {
	return pkix.Extension{Id: oid, Critical: critical, Value: value}
}

func CheckCert(cert *x509v2.Certificate, privPath string, pwd []byte) error {

	priv, err := GetPrivKey(privPath, pwd)
	if err != nil {
		log.Debugf("get sign privekey was error, error = %s", err)
		return err
	}
	if err = checkPub(cert, priv.PublicKey()); err != nil {
		log.Debugf("check cert pub was error, error = %s", err)
		return err
	}

	return nil
}

func GetPrivKey(path string, pwd []byte) (v2c.PrivateKey, error) {

	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	content, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	priv, err := as.PrivateKeyFromPEM(content, pwd)
	if err != nil {
		return nil, err
	}
	return priv, nil

}
func GetEnckey(path string) ([]byte, error) {
	pubkeyExt, err := GetPubKey(path)
	if err != nil {
		return nil, err
	}

	encKey, err := pubkeyExt.(v2c.EncryptKey).String()
	if err != nil {
		return nil, err
	}
	return []byte(encKey), nil
}

// getsign pub key
func GetPubKey(path string) (v2c.PublicKey, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	content, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	pub, err := as.PublicKeyFromPEM(content)
	if err != nil {
		return nil, err
	}

	return pub, nil
}

func getSignatureAlgorithm(privKey v2c.PrivateKey) (x509.SignatureAlgorithm, error) {
	if privKey == nil || privKey.PublicKey() == nil {
		return 0, errors.New("nil key material")
	}

	signatureAlgorithm := x509.UnknownSignatureAlgorithm
	switch privKey.PublicKey().ToStandardKey().(type) {
	case *rsa.PublicKey:
		signatureAlgorithm = x509.SHA256WithRSA
	case *sm2.PublicKey:
		signatureAlgorithm = x509.SignatureAlgorithm(x509v2.SM3WithSM2)
	case *ecdsa.PublicKey:
		signatureAlgorithm = x509.ECDSAWithSHA256
	default:
		return 0, errors.New("not support signature algorithm")
	}

	return signatureAlgorithm, nil
}

func checkPub(cert *x509v2.Certificate, pubKey v2c.PublicKey) error {
	if cert == nil || cert.PublicKey == nil {
		return errors.New("nil key material")
	}

	stdPub, err := cert.PublicKey.Bytes()
	if err != nil {
		return err
	}
	pub, err := pubKey.Bytes()
	if err != nil {
		return err
	}

	if string(stdPub) == string(pub) {
		return nil
	}

	return errors.New("check cert error")
}

/*
	int getKeyWord(unsigned char *keyWord, bool isInit);
	get key word  by enclave generate
*/
func GetKeyWord(isSim bool) ([]byte, error) {
	/*
		TODO: for sim module
	*/
	if isSim {
		keyByte := []byte("1234567890")
		err := ioutil.WriteFile("../../configs/sgxkeyrequest.dat", keyByte, 0644)
		if err != nil {
			panic(err)
		}
		return keyByte, nil
	}

	isInit := true
	if Exists(PwdFile) {
		isInit = false
	}

	var keyWord [16]C.uint8_t

	errInt := C.getKeyWord(&keyWord[0], C.bool(isInit))
	if errInt != 0 {
		return nil, errors.New("get key word error")
	}

	keyByte := (*[16]byte)(unsafe.Pointer(&keyWord[0]))[:16:16]
	if keyByte == nil {
		return nil, errors.New("keyWord is empty")
	}
	return keyByte, nil

}

func ParseCert(certPath string) (*x509v2.Certificate, error) {
	if !Exists(certPath) {
		file, err := os.Create(certPath)
		if err != nil {
			return nil, err
		}
		defer file.Close()
	}

	for {
		// check the file
		content, err := ReadFile(certPath)
		if err != nil {
			continue
		}

		block, _ := pem.Decode(content)
		if block == nil {
			//to do log
			time.Sleep(time.Second * 3)
			hint := "waiting in_teecert.pem load"
			log.Info(hint)
			fmt.Println(hint)
			continue
		}
		cert, err := x509v2.ParseCertificate(block.Bytes)
		if err == nil {
			return cert, nil
		}

	}

}
