/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package info

import (
	"crypto"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	"os"
	"testing"

	"crypto/rsa"
	"time"

	//crypto2 "crypto"

	v2c "chainmaker.org/chainmaker/common/v2/crypto"
	x509v2 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"github.com/stretchr/testify/require"
)

var (
	pwdword  = []byte("213123")
	rootCert = "./cert.pem"
	keypem   = "./key.pem"
)

func init() {
	TeeCsrFile = "../../configs/out_csr.pem"
	TeeCertFile = "../../configs/in_teecert.pem"
	TeeReportFile = "../../configs/out_report.dat"

	//签名公私钥
	SecreteKeyFile = "../../configs/secretkey.pem"
	PublickeyFile = "../../configs/publickey.pem"

	//加解密公私钥
	SecretkeyExt = "../../configs/secretkeyExt.pem"
	PublickeyExt = "../../configs/publickeyExt.pem"

}

//todo  test file
func TestCheckPub(t *testing.T) {
	deleteFile()
	genCsr()
	GenCsrToCert()
	//get cert
	content, _ := ReadFile(TeeCertFile)
	fmt.Println(TeeCertFile)
	cert, rest := pem.Decode(content)
	fmt.Println(rest)
	block, err := x509v2.ParseCertificate(cert.Bytes)
	if err != nil {
		panic(err)
	}
	// get ecnlave pwdword

	priv, err := GetPrivKey(SecreteKeyFile, pwdword)
	if err != nil {
		panic(err)
	}
	fmt.Println(priv.PublicKey().ToStandardKey())
	err = checkPub(block, priv.PublicKey())
	if err != nil {
		panic(err)
	}
	require.NoError(t, err)
}

func TestGetPriKey(t *testing.T) {

	fmt.Println("--------start-------")
	deleteFile()

	err := GenAlgoKey(SecretkeyExt, PublickeyExt, pwdword, v2c.RSA1024, true)
	algo := v2c.KeyType2NameMap[v2c.RSA512]
	if err != nil {
		fmt.Println(algo + "------error: ")
	}

	priv, err := GetPrivKey(SecretkeyExt, pwdword)
	if err != nil {
		fmt.Println(err)
		fmt.Println(priv)
	}
	m, err := priv.(v2c.DecryptKey).EncryptKey().Bytes()
	require.NoError(t, err)
	k, err := priv.PublicKey().Bytes()
	fmt.Println(k)
	fmt.Println(m)
	require.Equal(t, m, k)
	fmt.Println("success")
	fmt.Println("--------end---------")
	require.NoError(t, err)
}

func TestGetPubKey(t *testing.T) {

	fmt.Println("--------start-------")
	deleteFile()
	// get public key

	err := GenAlgoKey(SecreteKeyFile, PublickeyFile, pwdword, v2c.RSA1024, true)
	algo := v2c.KeyType2NameMap[v2c.RSA512]
	if err != nil {
		fmt.Println(algo + "------error: ")
	}
	pubkeyExt, err := GetPubKey(PublickeyFile)
	if err != nil {
		fmt.Println(err)
		panic("error")
	}
	l, err := pubkeyExt.(v2c.EncryptKey).Bytes()
	require.NoError(t, err)
	bytesPub, err := pubkeyExt.String()

	fmt.Println(l)
	fmt.Println(bytesPub)
	require.NoError(t, err)
	fmt.Println("--------end---------")
}

func TestGetEncPubKey(t *testing.T) {

	fmt.Println("--------start-------")
	deleteFile()
	// get public key

	err := GenAlgoKey(SecretkeyExt, PublickeyExt, pwdword, v2c.RSA1024, true)
	algo := v2c.KeyType2NameMap[v2c.RSA512]
	if err != nil {
		fmt.Println(algo + "------error: ")
	}
	pubkeyExt, err := GetEnckey(PublickeyExt)
	if err != nil {
		fmt.Println(err)
		panic("error")
	}
	require.NoError(t, err)
	fmt.Println(pubkeyExt)
	fmt.Println("--------end---------")
}

func TestGetCsr(t *testing.T) {

	fmt.Println("--------start-------")
	deleteFile()
	genCsr()
	fmt.Println("success")
	fmt.Println("--------end---------")
}

func genCsr() {
	fmt.Println("--------start-------")
	// get public key
	pwd := []byte("213123")
	err := GenAlgoKey(SecreteKeyFile, PublickeyExt, pwd, v2c.RSA1024, false)
	algo := v2c.KeyType2NameMap[v2c.RSA512]
	if err != nil {
		fmt.Println(algo + "------error: ")
	}
	pubkeyExt, err := GetPubKey(PublickeyExt)
	if err != nil {
		fmt.Println(err)
		panic("error")
	}

	pub, _ := pubkeyExt.String()

	priv, _ := GetPrivKey(SecreteKeyFile, pwd)
	if _, err = NewCsr([]byte(pub), priv); err != nil {
		fmt.Println("csr error ", err)
		panic(err)
	}
	fmt.Println("success")
	fmt.Println("--------end---------")
}

func TestGenAlgoKey(t *testing.T) {

	fmt.Println("--------start-------")
	keyword := []byte("213123")
	deleteFile()

	// i 对应不同的加密算法
	for i := 2; i <= 10; i++ {
		algo := v2c.KeyType2NameMap[v2c.KeyType(i)]

		fmt.Printf("%s------start-----type = %d \n", algo, v2c.KeyType(i))
		err := GenAlgoKey(SecretkeyExt, PublickeyExt, keyword, v2c.KeyType(i), false)
		if err != nil {
			fmt.Println(algo + "------error: ")
		}
		fmt.Println(algo + "------end-----")
		deleteFile()
	}
	fmt.Println("success")
	fmt.Println("--------end---------")
}

// delete pem file for unit test
func TestDeletePemFile(t *testing.T) {
	deleteFile()
}

func deleteFile() {
	var tmp []string = []string{
		TeeCsrFile,
		TeeCertFile,
		TeeReportFile,

		//签名公私钥
		SecreteKeyFile,
		PublickeyFile,

		//加解密公私钥
		SecretkeyExt,
		PublickeyExt,
		"../../configs/sgxkeyrequest.dat",
	}
	for _, v := range tmp {
		if Exists(v) {
			err := os.Remove(v) //删除文件test.txt
			if err != nil {
				fmt.Println(err)
			}
		}
	}

}


func TestGenEnclaveCert(t *testing.T) {
	deleteFile()
	// get public key
	pwd := []byte("213123")
	err := GenAlgoKey(SecreteKeyFile, PublickeyExt, pwd, v2c.RSA1024, false)
	algo := v2c.KeyType2NameMap[v2c.RSA512]
	if err != nil {
		fmt.Println(algo + "------error: ")
	}
	pubkeyExt, err := GetPubKey(PublickeyExt)
	if err != nil {
		fmt.Println(err)
		panic("error")
	}

	pub, _ := pubkeyExt.String()

	priv, _ := GetPrivKey(SecreteKeyFile, pwd)
	if _, err = NewCsr([]byte(pub), priv); err != nil {
		fmt.Println("csr error ", err)
		panic(err)
	}
	GenRootCert()
	GenCsrToCert()
}

func TestNewCertByCsr(t *testing.T) {
	GenCsrToCert()
}

func GenCsrToCert() {
	//get csr
	content, _ := ReadFile(TeeCsrFile)
	block, _ := pem.Decode(content)

	csr, _ := x509v2.ParseCertificateRequest(block.Bytes)
	fmt.Println(csr.Extensions)

	fmt.Println(csr.PublicKey.(crypto.PublicKey))
	//get cert
	content, _ = ReadFile(rootCert)
	block, _ = pem.Decode(content)
	cert, _ := x509.ParseCertificate(block.Bytes)

	//get key
	content, _ = ReadFile(keypem)
	block, _ = pem.Decode(content)
	key, _ := x509.ParsePKCS1PrivateKey(block.Bytes)

	//new
	max := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, _ := rand.Int(rand.Reader, max)

	// sonCert := &x509.Certificate{
	// 	SerialNumber: serialNumber,
	// 	PublicKey: cr.PublicKey,
	// 	Subject:            cr.Subject,
	// 	NotBefore:          time.Now(),
	// 	NotAfter:           time.Now().Add(365 * 24 * time.Hour),
	// 	SignatureAlgorithm: x509.SignatureAlgorithm(cr.SignatureAlgorithm),
	// 	ExtraExtensions:    cr.ExtraExtensions,
	// }
	notBefore := time.Now().Add(-10 * time.Minute).UTC()
	sonCert := &x509v2.Certificate{
		// Signature:             csr.Signature,
		// SignatureAlgorithm:    x509v2.SignatureAlgorithm(csr.SignatureAlgorithm),
		PublicKey:             csr.PublicKey,
		PublicKeyAlgorithm:    x509v2.PublicKeyAlgorithm(csr.PublicKeyAlgorithm),
		SerialNumber:          serialNumber,
		NotBefore:             notBefore,
		NotAfter:              notBefore.Add(time.Duration(1) * 365 * 24 * time.Hour).UTC(),
		BasicConstraintsValid: true,
		IsCA:                  true,
		//	Issuer:                csr.Subject,
		// KeyUsage: x509.KeyUsageDigitalSignature |
		// 	x509.KeyUsageKeyEncipherment |
		// 	x509.KeyUsageCertSign |
		// 	x509.KeyUsageCRLSign,

		ExtKeyUsage:     []x509.ExtKeyUsage{x509.ExtKeyUsageAny},
		Subject:         csr.Subject,
		ExtraExtensions: csr.Extensions,
	}
	k, _ := x509v2.ChainMakerCertToX509Cert(sonCert)
	derBytes, err := x509v2.CreateCertificate(rand.Reader, k, cert, csr.PublicKey.ToStandardKey(), key)
	if err != nil {
		panic(err)
	}

	certOut, err := os.Create(TeeCertFile)
	if err != nil {
		panic(err)
	}

	err = pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	if err != nil {
		panic(err)
	}
	certOut.Close()
}

// gen root cert for unit test
func GenRootCert() {
	max := new(big.Int).Lsh(big.NewInt(1), 128)   //把 1 左移 128 位，返回给 big.Int
	serialNumber, _ := rand.Int(rand.Reader, max) //返回在 [0, max) 区间均匀随机分布的一个随机值
	subject := pkix.Name{                         //Name代表一个X.509识别名。只包含识别名的公共属性，额外的属性被忽略。
		Organization:       []string{"Manning Publications Co."},
		OrganizationalUnit: []string{"Books"},
		CommonName:         "ChainMaker Super Team",
	}
	template := x509.Certificate{
		SerialNumber: serialNumber, // SerialNumber 是 CA 颁布的唯一序列号，在此使用一个大随机数来代表它
		Subject:      subject,
		NotBefore:    time.Now(),
		NotAfter:     time.Now().Add(365 * 24 * time.Hour),
		ExtKeyUsage:  []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth}, //证书用途
		KeyUsage:     x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign | x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,

		//IPAddresses:  []net.IP{net.ParseIP("127.0.0.1")},
		IsCA:                  true,
		BasicConstraintsValid: true,
	}
	pk, _ := rsa.GenerateKey(rand.Reader, 2048) //生成一对具有指定字位数的RSA密钥

	//CreateCertificate基于模板创建一个新的证书
	//第二个第三个参数相同，则证书是自签名的
	//返回的切片是DER编码的证书
	derBytes, _ := x509.CreateCertificate(rand.Reader, &template, &template, &pk.PublicKey, pk) //DER 格式
	certOut, _ := os.Create(rootCert)
	err := pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	if err != nil {
		panic(err)
	}
	certOut.Close()
	keyOut, _ := os.Create(keypem)
	err = pem.Encode(keyOut, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(pk)})
	if err != nil {
		panic(err)
	}
	keyOut.Close()
}

var (
	strRepot = "DhH///+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAAAAAAAAHAAAAAAAAAGRb1YAbGLqWULNp2h2zPnqB4cn4ljX8A2k9+tNFMHzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACD1xnnferKFHD2uvYqTXdDA8iZ22kCD5xw7h38CMfOngAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5h3iTzJrw6VVjJfM2k83mwAAAAAAAAAAAAAAAAAAAACj7B8gvAKO0TiLoH14G1JW"
)

func TestSaveReprt(t *testing.T) {
	err := saveReport(strRepot, TeeReportFile)
	if err != nil {
		panic(err)
	}
}

// // get test report
// func GenTestReport() {

// 	b, _ := base64.StdEncoding.DecodeString(strRepot)
// 	fmt.Println(b)
// 	ByteToStruct(unsafe.Pointer(&b))
// }


func TestGenRootCert(t *testing.T) {
	//deleteFile()
	GenRootCert()
}