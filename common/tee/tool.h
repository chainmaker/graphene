 /*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
 */


#include <stdio.h>
#include <string.h>
#include "sgx_arch.h"
#include "sgx_api.h"
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

ssize_t rw_file_f(const char* path, char* buf, size_t bytes, bool do_write);
int getReport(unsigned char* user_report_data_str, size_t data_len, sgx_report_t* report);
int getKeyWord(unsigned char *keyWord, bool isInit);